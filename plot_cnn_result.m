load('cnn.mat')

az = mean(auc);
az = squeeze(az);
[i,j]=find(az==max(max(az)));
a = squeeze(auc(:, i,:));
mean_velocity = mean(a);
std_velocity = std(a);

figure;
hold on
plot(500:500:10000,mean_velocity)
errorbar(500:500:10000,mean_velocity,std_velocity,'.')
