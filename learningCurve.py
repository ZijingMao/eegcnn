# -*- coding: utf-8 -*-
"""
Created on Sun Aug 16 18:53:37 2015

@author: zijing
"""
import scipy
import h5py
import numpy as np

# Make sure that caffe is on the python path:
caffe_root = '/home/zijing/caffe/'  # this file is expected to be in {caffe_root}/examples
caffe_example= 'examples/DLAutoTest/'
import sys
sys.path.insert(0, caffe_root + 'python')
sys.path.append(caffe_root)
from examples.DLAutoTest import mat2lmdb
from examples.DLAutoTest import transferModule
from examples.DLAutoTest import randomSearchGenerator

'''
This file is a learning curve result generator:
	the vertical axis is the AUC (0-100%), 
	the horizontal axis is the training sample size.
	training sample size changes each fold in log2 scale
'''        
        
'''
This training process is mainly for cross-subject test, each slot of variables
should store data from only one subject

dataIdx:    different feature types/datasets that used for classification

dataIdx:       dataIdxs for separating epochs into training/testing

idx:        different model that can be used for training/validation

data:       set your data file use the format 'dataIdx{dataIdx}.mat'
            six variables are contained:
                train_x     test_x      valid_x
                train_y     test_y      valid_y
            
train_x:    the variable contains one array, each cell is one dataIdx of 
            training data, it should be:
            [epochs * featureMaps * channels * timeSamples]
train_y:    the variable contains one array, each cell is one dataIdx of 
            training data, it should be:
            [epochs,]
test_x:     the variable contains one array, each cell is one dataIdx of 
            testing data, it should be:
            [epochs * featureMaps * channels * timeSamples]
test_y:     the variable contains one array, each cell is one dataIdx of 
            testing data, it should be:
            [epochs,]
valid_x:    the variable contains one array, each cell is one dataIdx of 
            validing data, it should be:
            [epochs * featureMaps * channels * timeSamples]
valid_y:    the variable contains one array, each cell is one dataIdx of 
            validing data, it should be:
            [epochs,]
'''

maxDataIdx = 1
maxRandomSearchNum = 16
nfeat = 1; height = 64; width = 128
for dataIdx in range(0, maxDataIdx):   
    # load data first
    data = '/home/zijing/Downloads/fold'+str(dataIdx+1)+'.mat'
    mat = scipy.io.loadmat(data)    
    train_x     =   mat['train_x']
    train_y     =   mat['train_y'][:,0]
    test_x      =   mat['test_x']
    test_y      =   mat['test_y'][:,0]
    valid_x     =   mat['valid_x']
    valid_y     =   mat['valid_y'][:,0]
    
#    f = h5py.File(data)
#    train_x = f[u"train_x"]
#    train_x = np.array(train_x)
#    train_y = f[u"train_y"]
#    train_y = np.array(train_y)
#    test_x  = f[u"test_x"]
#    test_x  = np.array(test_x)
#    test_y  = f[u"test_y"]
#    test_y  = np.array(test_y)
    
    epoch_shape_train   = train_x.shape
    epoch_shape_test    = test_x.shape
    if len(epoch_shape_train) == 2 and nfeat*height*width == epoch_shape_train[0]:
        # the data is stored in the 'feature * epoch' way
        epoch_len_train = epoch_shape_train[1]
        train_x = train_x.reshape([width, height, nfeat, epoch_len_train]).transpose()
        train_y = train_y[0,:]
        epoch_len_test = epoch_shape_test[1]
        test_x = test_x.reshape([width, height, nfeat, epoch_len_test]).transpose()
        test_y = test_y[0,:]
        
#    Inputs = []
#    Labels = []
#    with h5py.File(data) as f:
#        for element in f['Inputs'][0]:
#            print(element)
#            Inputs.append(f[element][:])
#        for element in f['Labels'][0]:    
#            Labels.append(f[element][:][:, 0])
            
    blob_dim = list(train_x.shape)
    # check the blob dimension with [featureMaps * channels * timeSamples]
    blob_dim.pop(0)
    
    ###########################################################################
    # select part of the training data
    randPos = np.random.permutation(len(train_y))
    y_train = train_y[randPos]
    x_train = train_x[randPos, :, :, :]
    for trainSizeIdx in range(6, 13):
        trainTargetIdx = np.where(y_train==1)[0]
        trainNontargetIdx = np.where(y_train==0)[0]
        trainSize = 2**trainSizeIdx
        
        # select the target and non-target
        trainIdx = np.concatenate((trainTargetIdx[0:trainSize], \
                                    trainNontargetIdx[0:trainSize]))
        train_y = y_train[trainIdx]
        train_x = x_train[trainIdx, :, :, :]
        
        ###########################################################################
        print('=' * 78)
        print('Current feature choose: ' + str(dataIdx))
        print('DNN as model')
        print('=' * 78)
        # get the random list to construct .prototxt
        iterations = 500
        scalar = 0.1
        maxLayerNum = 4
        topLayer = 2
        middleLayer = [100*2**j for j in reversed(range(0,maxLayerNum))]
        dropLayer = 0.5
        randLayer_str_list, randLayer_list=randomSearchGenerator.genRandomSearchList \
                (maxLayerNum, topLayer, middleLayer, dropLayer, maxRandomSearchNum)
        transferMod = transferModule.transferModule(caffe_root, caffe_example, 'mlp')        
        transferMod.edit_prototxt_iter('rand_solver', iterations)
            
        '''
        Do a random search for the DNN
        set dataIdx = 0 as the validation for model selection
        '''    
        # write to training and validation variable to lmdb file
        mat2lmdb.data2lmdb(train_x, valid_x, train_y, valid_y, dataIdx)
        
        auc_model_selection = np.zeros([maxRandomSearchNum, 20]) 
        for idx in range(0, maxRandomSearchNum):
            # first revise the training and testing prototxt
            randomSearchGenerator.constructPrototxt(randLayer_list[idx], \
                                'rand_train', 'rand_test', dataIdx, scalar, blob_dim)
            # then do training
            res = transferMod.train_one_model('rand_solver', 'rand_train', dataIdx)
            # then do testing
            # the result stored here
            auc = np.zeros([20])
            for i in range(1,21):
                auc[i-1]    =   transferMod.get_accuracy\
                                (train_x, valid_x, train_y, valid_y, \
                                'rand_test', dataIdx, scalar, iterations*i, 'auc')   
            print('Model'+str(idx)+' tested.')
            auc_model_selection[idx] = auc
                     
        # after train models, get the best model and iterations
        curr_model,curr_iter = \
            np.unravel_index(auc_model_selection.argmax(), auc_model_selection.shape)
        curr_iter = curr_iter+1
        print('We have found the best model is: ')
        print('=' * 78)
        print(randLayer_str_list[curr_model])    
        print('=' * 78)
        print('The iteration we use is: '+str((curr_iter+1)*iterations))
        print('=' * 78)
        
        auc_all = np.zeros([1, 20]) 
    
        # mat2lmdb.data2lmdb(train_x, test_x, train_y, test_y, dataIdx)
            
        randomSearchGenerator.constructPrototxt(randLayer_list[curr_model], \
                            'rand_train', 'rand_test', dataIdx, scalar, blob_dim)
        # then do training
        transferMod.train_one_model('rand_solver', 'rand_train', dataIdx)
        # then do testing
        # the result stored here
        auc = np.zeros([20])     
        for i in range(1,21):
            auc[i-1]    =   transferMod.get_accuracy\
                            (train_x, test_x, train_y, test_y, \
                            'rand_test', dataIdx, scalar, iterations*i, 'auc')   
    
        # auc_dnn_CS.append(randLayer_str_list[idx])
        # store all aucs
        auc_all[dataIdx] = auc          
        
        mod_all = randLayer_list[curr_model]    
        ite_all = iterations*curr_iter
        # write to mat file
        scipy.io.savemat(caffe_root+caffe_example+'dnn'+str(trainSize)+'Fold'+str(dataIdx)+'.mat', \
                        {'auc': auc_all, 'model': mod_all, 'iters': ite_all})
    
        print('=' * 78)   
        ########################################################################### 
        print('\n' * 2)
        ###########################################################################
        print('=' * 78)
        print('Current feature choose: ' + str(dataIdx))
        print('CNN-2Layer as model')
        print('=' * 78)    
        
        iterations = 500
        scalar = 0.1
        if dataIdx == 0:
            timefilter1 = [2**j for j in reversed(range(0,5))]
        else:
            timefilter1 = [2**j for j in reversed(range(0,3))]
        timefilter2 = [2**j for j in reversed(range(0,5))]
        filterNum = [5*j for j in reversed(range(1,6))]
        maxLayerNum = 3
        topLayer = 2
        middleLayer = [100*2**j for j in reversed(range(0,maxLayerNum))]
        dropLayer = 0.5
        randLayer_str_list, randLayer_list=randomSearchGenerator.genRandomSearchListCNN\
                                (timefilter1, timefilter2, filterNum, maxLayerNum, \
                                topLayer, middleLayer, dropLayer, maxRandomSearchNum, blob_dim)    
        # initialize transfer module for cnn
        transferMod = transferModule.transferModule(caffe_root, caffe_example, 'cnn')
        transferMod.edit_prototxt_iter('rand_solver', iterations)  
        
        # still use the previous inputs and labels
        '''
        Do a random search for the CNN-2Layer
        set dataIdx = 0 as the validation for model selection
        '''
        # write to training and validation variable to lmdb file
        # mat2lmdb.data2lmdb(train_x, valid_x, train_y, valid_y, dataIdx)
                
        auc_model_selection = np.zeros([maxRandomSearchNum, 20]) 
        for idx in range(0, maxRandomSearchNum):
            # first revise the training and testing prototxt
            randomSearchGenerator.constructPrototxtCNN(randLayer_list[idx], \
                                'rand_train', 'rand_test', dataIdx, scalar, blob_dim)
            # then do training
            res = transferMod.train_one_model('rand_solver', 'rand_train', dataIdx)
            # then do testing
            # the result stored here
            auc = np.zeros([20])
            for i in range(1,21):
                auc[i-1]    =   transferMod.get_accuracy\
                                (train_x, valid_x, train_y, valid_y, \
                                'rand_test', dataIdx, scalar, iterations*i, 'auc')   
            print('Model'+str(idx)+' tested.')
            auc_model_selection[idx] = auc  
        # after train models, get the best model and iterations
        curr_model,curr_iter = \
            np.unravel_index(auc_model_selection.argmax(), auc_model_selection.shape)
            
        print('We have found the best model is: ')
        print('=' * 78)
        print(randLayer_str_list[curr_model])    
        print('=' * 78)
        print('The iteration we use is: '+str((curr_iter+1)*iterations))
        print('=' * 78)
        
        auc_all = np.zeros([1, 20]) 
        '''
        Do a test for the CNN
        '''
        # mat2lmdb.data2lmdb(train_x, test_x, train_y, test_y, dataIdx)
            
        randomSearchGenerator.constructPrototxtCNN(randLayer_list[curr_model], \
                            'rand_train', 'rand_test', dataIdx, scalar, blob_dim)
        # then do training
        transferMod.train_one_model('rand_solver', 'rand_train', dataIdx)
        # then do testing
        # the result stored here
        auc = np.zeros([20])     
        for i in range(1,21):
            auc[i-1]    =   transferMod.get_accuracy\
                            (train_x, test_x, train_y, test_y, \
                            'rand_test', dataIdx, scalar, iterations*i, 'auc')   
    
        # auc_dnn_CS.append(randLayer_str_list[idx])
        # store all aucs
        auc_all[dataIdx] = auc          
        
        mod_all = randLayer_list[curr_model]    
        ite_all = iterations*curr_iter
        # write to mat file
        scipy.io.savemat(caffe_root+caffe_example+'cnn2L'+str(trainSize)+'Fold'+str(dataIdx)+'.mat', \
                        {'auc': auc_all, 'model': mod_all, 'iters': ite_all})
    
        
        print('=' * 78)
        ###########################################################################
        print('\n' * 2)
        ###########################################################################
        print('=' * 78)
        print('Current feature choose: ' + str(dataIdx))
        print('CNN-1Layer as model')
        print('=' * 78)    
        
        iterations = 500
        if dataIdx == 0:
            scalar = 0.1
        else:
            scalar = 0.1
        if dataIdx == 0:
            timefilter1 = [2**j for j in reversed(range(0,5))]
        else:
            timefilter1 = [2**j for j in reversed(range(0,5))]    
        timefilter2 = []
        filterNum = [5*j for j in reversed(range(1,6))]
        maxLayerNum = 3
        topLayer = 2
        middleLayer = [100*2**j for j in reversed(range(0,maxLayerNum))]
        dropLayer = 0.5
        randLayer_str_list, randLayer_list=randomSearchGenerator.genRandomSearchListCNN\
                                (timefilter1, timefilter2, filterNum, maxLayerNum, \
                                topLayer, middleLayer, dropLayer, maxRandomSearchNum, blob_dim)    
        # initialize transfer module for cnn
        transferMod = transferModule.transferModule(caffe_root, caffe_example, 'cnn')
        transferMod.edit_prototxt_iter('rand_solver', iterations)  
        
        # still use the previous inputs and labels
        '''
        Do a random search for the CNN-1Layer
        set dataIdx = 0 as the validation for model selection
        '''
        # write to training and validation variable to lmdb file
        # mat2lmdb.data2lmdb(train_x, valid_x, train_y, valid_y, dataIdx)
                
        auc_model_selection = np.zeros([maxRandomSearchNum, 20]) 
        for idx in range(0, maxRandomSearchNum):
            # print(str(idx))
            # first revise the training and testing prototxt
            randomSearchGenerator.constructPrototxtCNN(randLayer_list[idx], \
                                'rand_train', 'rand_test', dataIdx, scalar, blob_dim)
            # then do training
            res = transferMod.train_one_model('rand_solver', 'rand_train', dataIdx)
            # then do testing
            # the result stored here
            auc = np.zeros([20])
            for i in range(1,21):
                auc[i-1]    =   transferMod.get_accuracy\
                                (train_x, valid_x, train_y, valid_y, \
                                'rand_test', dataIdx, scalar, iterations*i, 'auc')   
            auc_model_selection[idx] = auc  
        # after train models, get the best model and iterations
        curr_model,curr_iter = \
            np.unravel_index(auc_model_selection.argmax(), auc_model_selection.shape)
            
        print('We have found the best model is: ')
        print('=' * 78)
        print(randLayer_str_list[curr_model])    
        print('=' * 78)
        print('The iteration we use is: '+str((curr_iter+1)*iterations))
        print('=' * 78)
        
        auc_all = np.zeros([1, 20])  
        '''
        Do a test for the CNN
        '''
        # mat2lmdb.data2lmdb(train_x, test_x, train_y, test_y, dataIdx)
            
        randomSearchGenerator.constructPrototxtCNN(randLayer_list[curr_model], \
                            'rand_train', 'rand_test', dataIdx, scalar, blob_dim)
        # then do training
        transferMod.train_one_model('rand_solver', 'rand_train', dataIdx)
        # then do testing
        # the result stored here
        auc = np.zeros([20])     
        for i in range(1,21):
            auc[i-1]    =   transferMod.get_accuracy\
                            (train_x, test_x, train_y, test_y, \
                            'rand_test', dataIdx, scalar, iterations*i, 'auc')   
    
        # auc_dnn_CS.append(randLayer_str_list[idx])
        # store all aucs
        auc_all[dataIdx] = auc          
        
        mod_all = randLayer_list[curr_model]    
        ite_all = iterations*curr_iter
        # write to mat file
        scipy.io.savemat(caffe_root+caffe_example+'cnn1L'+str(trainSize)+'Fold'+str(dataIdx)+'.mat', \
                        {'auc': auc_all, 'model': mod_all, 'iters': ite_all})
                        
                        
        print('=' * 78)
        ###########################################################################
    
        ###########################################################################
        print('=' * 78)
        print('Current feature choose: ' + str(dataIdx))
        print('CNN-1SF1TF as model')
        print('=' * 78)    
        
        iterations = 500
        if dataIdx == 0:
            scalar = 0.1
        else:
            scalar = 0.1
        transferMod = transferModule.transferModule(caffe_root, caffe_example, 'cnn')    
        transferMod.edit_prototxt_iter('rand_solver_1sf_1tf', iterations)    
    
        ''' modify model in file:
            rand_solver_1sf_1tf.prototxt
            rand_test_1sf_1tf.prototxt
            rand_train_1sf_1tf.prototxt
        '''
        auc_all = np.zeros([1, 20]) 
        # mat2lmdb.data2lmdb(train_x, test_x, train_y, test_y, dataIdx)
                
        transferMod.train_baseline_model('rand_solver_1sf_1tf', \
                                         'rand_train_1sf_1tf', dataIdx, scalar)
        auc = np.zeros([20])
        transferMod.edit_model_test('rand_test_1sf_1tf', blob_dim)
        for i in range(1,21):
            auc[i-1]    =   transferMod.get_accuracy\
                            (train_x, test_x, train_y, test_y, \
                            'rand_test_1sf_1tf', dataIdx, scalar, iterations*i, 'auc')   
        # store all aucs
        auc_all[dataIdx] = auc 
            
        # write to mat file
        scipy.io.savemat(caffe_root+caffe_example+'cnn1sf_1tf'+str(trainSize)+'Fold'+str(dataIdx)+'.mat', \
                        {'auc': auc_all})
    
        print('=' * 78)   
        ########################################################################### 
        print('\n' * 2)
        ###########################################################################
        print('=' * 78)
        print('Current feature choose: ' + str(dataIdx))
        print('CNN-2TF as model')
        print('=' * 78)    
        
        iterations = 500
        if dataIdx == 0:
            scalar = 0.1
        else:
            scalar = 0.1
        transferMod = transferModule.transferModule(caffe_root, caffe_example, 'cnn')    
        transferMod.edit_prototxt_iter('rand_solver_2tf', iterations)    
    
        ''' modify model in file:
            rand_solver_2tf.prototxt
            rand_test_2tf.prototxt
            rand_train_2tf.prototxt
        '''
        auc_all = np.zeros([1, 20]) 
        # mat2lmdb.data2lmdb(train_x, test_x, train_y, test_y, dataIdx)
                
        transferMod.train_baseline_model('rand_solver_2tf', \
                                         'rand_train_2tf', dataIdx, scalar)
        auc = np.zeros([20])
        transferMod.edit_model_test('rand_test_2tf', blob_dim)
        for i in range(1,21):
            auc[i-1]    =   transferMod.get_accuracy\
                            (train_x, test_x, train_y, test_y, \
                            'rand_test_2tf', dataIdx, scalar, iterations*i, 'auc')   
        # store all aucs
        auc_all[dataIdx] = auc 
            
        # write to mat file
        scipy.io.savemat(caffe_root+caffe_example+'cnn2tf'+str(trainSize)+'Fold'+str(dataIdx)+'.mat', \
                        {'auc': auc_all})
                        
        print('=' * 78)   
        ###########################################################################
        print('\n' * 2)
