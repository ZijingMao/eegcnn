# -*- coding: utf-8 -*-
"""
Created on Sat Aug 15 17:25:49 2015

@author: zijing
"""

import lmdb
import numpy as np
import math

# Make sure that caffe is on the python path:
caffe_root = '/home/zijing/caffe/'  # this file is expected to be in {caffe_root}/examples
caffe_example= 'examples/DLAutoTest/'
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe

''' 
The data is in the format with variable name:
    train_x, test_x, train_y, test_y
    x is feature, y is label

The data size should be feature: 4-D, label: 1-D

The feature dimension should be:
    [epochs * featureMaps * channels * timeSamples]

The label dimension should be:
    [epochs,]
'''
def generate_one_fold_lmdb(Inputs, Labels, fold, step = 1):    
    subLen = len(Labels)    # check how many subject size
    
    # define test and train idx
    testIdx = range((fold)*step, (fold+1)*step)
    trainIdx = range(0, subLen)
    for i in sorted(testIdx, reverse=True):
        trainIdx.pop(i)    
        
    # print train and test information
    print('Testing Subjects: '+str(testIdx))
    print('Training Subjects: '+str(trainIdx))
    
    # get blob information
    blob_dim = list(Inputs[0].shape)
    # check the blob dimension with [featureMaps * channels * timeSamples]
    blob_dim.pop(0)
    
    train_x = np.array([],dtype=np.float64).reshape((0,) + tuple(blob_dim))
    train_y = []
    test_x = np.array([],dtype=np.float64).reshape((0,) + tuple(blob_dim))
    test_y = []
    for subID in trainIdx:
        # concat subtrain to train
        train_x = np.concatenate((train_x, Inputs[subID]), axis=0)
        train_y = np.append(train_y, Labels[subID])
    for subID in testIdx:
        # concat subtest to test
        test_x  = np.concatenate((test_x, Inputs[subID]), axis=0)
        test_y  = np.append(test_y, Labels[subID])
        # finish print log

    print('Finish cat train and test fold ' + str(fold))
        
    data2lmdb(train_x, test_x, train_y, test_y, fold)
    
    return train_x, test_x, train_y, test_y

def data2lmdb(train_x, test_x, train_y, test_y, fold, prefix='_lmdb_'):
    # do random permutation
    randPos = np.random.permutation(len(train_y))
    train_y = train_y[randPos]
    train_x = train_x[randPos, :, :, :]
    
    lmdb_train_data_name    = caffe_root+caffe_example+'train'+prefix+str(fold)
    lmdb_test_data_name     = caffe_root+caffe_example+'test'+prefix+str(fold)
    print('Writing train labels and data\n')
    input2lmdb(train_x, train_y,    lmdb_train_data_name)
    print('Writing test labels and data\n')
    input2lmdb(test_x,  test_y,     lmdb_test_data_name)
    print('')
    

def input2lmdb(Inputs, Labels, lmdb_data_name):
    # Size of buffer: 1000 elements to reduce memory consumption
    # uniqueLabelSize = len(np.unique(Labels))
    for idx in range(int(math.ceil(Inputs.shape[0]/1000.0))):   # choose how many epochs
        tmpInputs   = Inputs[(1000*idx):(1000*(idx+1)), :, :, :]
        tmpLabels   = Labels[(1000*idx):(1000*(idx+1))]
        in_db_data  = lmdb.open(lmdb_data_name, map_size=int(1e12))
        with in_db_data.begin(write=True) as in_txn:
    		for in_idx in range(0, tmpInputs.shape[0]):
    			im       = tmpInputs[in_idx, :, :, :]
    			lbl      = tmpLabels[in_idx]
    			im_dat   = caffe.io.array_to_datum(im.astype(float), lbl.astype(int))
    			in_txn.put('{:0>10d}'.format(1000*idx + in_idx), im_dat.SerializeToString())
    			string_  = str(1000*idx+in_idx+1) + ' / ' + str(Inputs.shape[0])
    			sys.stdout.write("\r%s" % string_)
    			sys.stdout.flush()
        in_db_data.close()
    print('')
