import scipy
import matplotlib.pyplot as plt
import numpy as np

caffe_root = '/home/zijing/caffe/'  # this file is expected to be in {caffe_root}/examples
caffe_example= 'examples/DLAutoTest/'
import sys
sys.path.insert(0, caffe_root + 'python')
sys.path.append(caffe_root)
import caffe

'''
This file is a feature visulization tool by reading trained cnn models:

	only visualization for forward step has been implemented
	backward feature generator will be implemented in the future...
'''  

def vis_ip(layer, title):
    fig, ax = plt.subplots()
    plt.plot(np.transpose(layer))
    plt.title(title, fontsize=20)
    fig.savefig(caffe_root+caffe_example+title+'.pdf')
    plt.show()
    plt.close()

# take an array of shape (n, height, width) or (n, height, width, channels)
# and visualize each (height, width) thing in a grid of size approx. sqrt(n) by sqrt(n)
def vis_square(data, title, n1, n2, padsize=1, padval=0):
#    data= c2
#    data = data.transpose(0, 2, 1, 3)
    data -= data.min()
    data /= data.max()
    
    data = data.reshape((n1, n2) + data.shape[1:]).transpose(0, 3, 1, 4, 2) 
    
    # data = np.squeeze(data)
    # force the number of filters to be square
    padding = ((0, 0), (0, padsize), (0, 0)) + ((0, padsize), (0, 0)) 
    data = np.pad(data, padding, mode='constant', constant_values=(padval, padval))
    
    # tile the filters into an image
    # data = data.reshape((n1, n2) + data.shape[1:]).transpose((0, 2, 1, 3) + tuple(range(4, data.ndim + 1)))
    data = data.reshape((data.shape[0] * data.shape[1], data.shape[2] * data.shape[3]) + data.shape[4:])
        
    fig, ax = plt.subplots()
    plt.title(title, fontsize=20)
    ax.imshow(np.squeeze(data))
    # axis
#    xstep = data.shape[1]/n1
#    ystep = data.shape[0]/n2
    ax.set_xticks([], minor=False)
    ax.set_yticks([], minor=False)
    ax.tick_params(labelsize=20)
    # labels
    # row_labels   =   list(['1', '32', '64'])
    # col_labels   =   list(['1', '10', '20', '30', '40'])
    # ax.set_xticklabels(col_labels, minor=False)
    # ax.set_yticklabels(row_labels, minor=False)
    fig.savefig(caffe_root+caffe_example+title+'.pdf')
    plt.show()
    plt.close()

def vis_data(data, plotTitle):
    filt_min = np.min(nontarget.min(), target.min())
    filt_max = np.max(nontarget.max(), target.max())
    
    fig, ax = plt.subplots()
    plt.title(plotTitle, fontsize=20)
    im = ax.imshow(data, vmin=filt_min, vmax=filt_max)
    # axis
    ax.set_xticks(np.arange(0, data.shape[1]+1, 32), minor=False)
    ax.set_yticks(np.arange(0, data.shape[0]+1, 32), minor=False)
    ax.tick_params(labelsize=20)
    # labels
    row_labels   =   list(['1', '32', '64'])
    col_labels   =   list(['0', '0.25', '0.5', '0.75', '1'])
    ax.set_xticklabels(col_labels, minor=False)
    ax.set_yticklabels(row_labels, minor=False)
    # colorbar
    cbar = plt.colorbar(im,fraction=0.024, pad=0.04)
    cbar.set_ticks([filt_min, 0, filt_max])
    cbar.set_ticklabels([str(round(filt_min, 2)),'0',str(round(filt_max, 2))])
    cbar.ax.tick_params(labelsize=20)
    
    fig.savefig(caffe_root+caffe_example+plotTitle+'.pdf')
    plt.show()
    plt.close()

'''
define the model, the prototxt, train_x, train_y, test_x, test_y

notice inputs should be 4-D
'''
PRETRAINED = '/home/zijing/caffe/examples/DLAutoTest/CS0ct_cnn0_iter_5000.caffemodel'
MODEL_FILE = '/home/zijing/caffe/examples/DLAutoTest/rand_test.prototxt'

demo_data = scipy.io.loadmat('/home/zijing/caffe/examples/DLAutoTest/demo.mat');
train_x     =   demo_data['train_x'];
train_y     =   demo_data['train_y'];
test_x      =   demo_data['test_x'];
test_y      =   demo_data['test_y'];

'''
build and forward the trained model
'''
net = caffe.Net(MODEL_FILE, PRETRAINED, caffe.TEST)

nontarget   = test_x[0,0,:,:]
# nontarget   = np.squeeze(np.mean(nontarget, axis=0))
vis_data(nontarget, 'nontarget input')

target      = test_x[4]
target      = np.squeeze(np.mean(target, axis=0))
vis_data(target, 'target input')


nontargetOut = net.forward(data = nontarget[np.newaxis, np.newaxis, :, :])
c1 = net.blobs['c1'].data
vis_square(c1.transpose(0, 2, 1, 3), 'nontarget Convolution Layer 1', 1, 1)   
p1 = net.blobs['p1'].data
vis_square(p1.transpose(0, 2, 1, 3), 'nontarget Pooling Layer 1', 1, 1)  
c2 = net.blobs['c2'].data
vis_square(c2.transpose(0, 2, 1, 3), 'nontarget Convolution Layer 2', 1, 1)   
p2 = net.blobs['p2'].data
vis_square(p2.transpose(0, 2, 1, 3), 'nontarget Pooling Layer 2', 1, 1)  
l1 = net.blobs['l1'].data
vis_ip(l1, 'nontarget l1')
l2 = net.blobs['l2'].data
vis_ip(l2, 'nontarget l2')

out = nontargetOut['prob']
width = 0.35
ind = np.arange(2)
fig, ax = plt.subplots()
plt.bar(ind, tuple(out[0,:]), width)
plt.ylabel('Probability', fontsize=20)
plt.title('Output', fontsize=20)
plt.xticks(ind+width/2., ('Nontarget', 'Target'), fontsize=20)
plt.yticks(np.arange(0,1,0.20), fontsize=20)
fig.savefig(caffe_root+caffe_example+'nontarget prob.pdf')


targetOut = net.forward(data = target[np.newaxis, np.newaxis, :, :])
c1 = net.blobs['c1'].data
vis_square(c1.transpose(0, 2, 1, 3), 'target Convolution Layer 1', 1, 1)   
p1 = net.blobs['p1'].data
vis_square(p1.transpose(0, 2, 1, 3), 'target Pooling Layer 1', 1, 1)  
c2 = net.blobs['c2'].data
vis_square(c2.transpose(0, 2, 1, 3), 'target Convolution Layer 2', 1, 1)   
p2 = net.blobs['p2'].data
vis_square(p2.transpose(0, 2, 1, 3), 'target Pooling Layer 2', 1, 1)  
l1 = net.blobs['l1'].data
vis_ip(l1, 'target l1')
l2 = net.blobs['l2'].data
vis_ip(l2, 'target l2')

out = targetOut['prob']
width = 0.35
ind = np.arange(2)
fig, ax = plt.subplots()
plt.bar(ind, tuple(out[0,:]), width)
plt.ylabel('Probability', fontsize=20)
plt.title('Output', fontsize=20)
plt.xticks(ind+width/2., ('Nontarget', 'Target'), fontsize=20)
plt.yticks(np.arange(0,1,0.20), fontsize=20)
fig.savefig(caffe_root+caffe_example+'target prob.pdf')

# weight figure are saved here
c1 = net.params['c1'][0].data   # get the idx=0 is weight, idx=1 is bias
vis_square(c1.transpose(0, 1, 2, 3), 'Convolution Layer 1 (Weight)', 1, 40)
                
c2 = net.params['c2'][0].data   # get the idx=0 is weight, idx=1 is bias
vis_square(c2.transpose(0, 2, 3, 1), 'Convolution Layer 2 (Weight)', 5, 4)

# save c1, c2 into mat file
scipy.io.savemat(caffe_root+caffe_example+'demo.filterWeights.mat', \
                {'c1': c1, 'c2': c2})
