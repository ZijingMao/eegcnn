# -*- coding: utf-8 -*-
"""
Created on Wed Aug 26 10:36:30 2015

@author: zijing
"""

# generate all lmdb
import subprocess
import numpy as np
import sklearn.metrics as metrics

class transferModule:
    
    # Make sure that caffe is on the python path:
    # self.caffe_root = '/home/zijing/caffe/'  # this file is expected to be in {self.caffe_root}/examples
    # self.caffe_example = 'examples/CT2WS/'
    
    def __init__(self, caffe_root= '/home/zijing/caffe/', caffe_example= 'examples/DLAutoTest/', module_type = 'cnn'):
        self.caffe_root = caffe_root
        self.caffe_example = caffe_example
        self.module_type = module_type
    
        import sys
        sys.path.insert(0, self.caffe_root + 'python')
        sys.path.append(self.caffe_root)
    
    def edit_prototxt_iter(self, eeg_conv_solver, iterations):
        
        # then training
           
        # first editing prototxt
        idx = 0
        idx_max_iter_snapshot = 20
        idx_each_iter_snapshot = 22
        with open(self.caffe_root+self.caffe_example+eeg_conv_solver+".prototxt", "r+") as f:
            # read files
            d = f.readlines()
            f.seek(0)
            for line in d:
                idx = idx + 1
                # read snapshot line
                if idx == idx_max_iter_snapshot:
                    f.write('max_iter: '+str(iterations*20)+'\n')  
                elif idx == idx_each_iter_snapshot:
                    f.write('snapshot: '+str(iterations)+'\n')  
                else:
                    f.write(line)
                          
    
    def edit_prototxt(self, eeg_conv_solver, eeg_train_test_conv, fold):
        
        # then training
           
        # first editing prototxt
        idx = 0
        idx_info_net = 2
        idx_info_snapshot = 23
        with open(self.caffe_root+self.caffe_example+eeg_conv_solver+".prototxt", "r+") as f:
            # read files
            d = f.readlines()
            f.seek(0)
            for line in d:
                idx = idx + 1
                # read snapshot line
                if idx == idx_info_net:
                    f.write('net: "'+self.caffe_root+self.caffe_example+\
                             eeg_train_test_conv+'.prototxt"\n') 
                elif idx == idx_info_snapshot:
                    f.write('snapshot_prefix: "'+self.caffe_root+self.caffe_example+\
                             self.module_type+str(fold)+'"\n')    
                else :
                    f.write(line)
                    
    def edit_model_test(self, eeg_train_test_conv, blob_dim):
        
        idx = 0
        idx_info_featmap = 4
        idx_info_channel = 5
        idx_info_time = 6
        with open(self.caffe_root+self.caffe_example+eeg_train_test_conv+".prototxt", "r+") as f:
            # read files
            d = f.readlines()
            f.seek(0)
            for line in d:
                idx = idx + 1
                # read snapshot line
                if idx == idx_info_featmap:
                    f.write('input_dim: '+str(blob_dim[0])+'\n')     
                elif idx == idx_info_channel:
                    f.write('input_dim: '+str(blob_dim[1])+'\n')     
                elif idx == idx_info_time:
                    f.write('input_dim: '+str(blob_dim[2])+'\n')       
                else:
                    f.write(line)        
    
    def edit_model(self, eeg_train_test_conv, fold, scalar, prefix='_lmdb_'):
        
        idx = 0
        idx_info_train = 14
        idx_info_test = 31
        scalar_info_train = 11
        scalar_info_test = 28
        with open(self.caffe_root+self.caffe_example+eeg_train_test_conv+".prototxt", "r+") as f:
            # read files
            d = f.readlines()
            f.seek(0)
            for line in d:
                idx = idx + 1
                # read snapshot line
                if idx == scalar_info_train:
                    f.write('    scale: '+str(scalar)+'\n')     
                elif idx == idx_info_train:
                    f.write('    source: "'+self.caffe_root+self.caffe_example+\
                                'train'+prefix+str(fold)+'"\n')       
                elif idx == scalar_info_test:
                    f.write('    scale: '+str(scalar)+'\n')    
                elif idx == idx_info_test:
                    f.write('    source: "'+self.caffe_root+self.caffe_example+\
                                'test'+prefix+str(fold)+'"\n')      
                else:
                    f.write(line)    
    
    def transfer_one_model(self, eeg_conv_solver, eeg_train_test_conv, caffe_model,\
                            prefix, subID, fold, offset, scalar):
        # then training
           
        # first editing prototxt
        self.edit_prototxt(eeg_conv_solver, eeg_train_test_conv, fold)
                            
        # second editing conv prototxt
        self.edit_model(eeg_train_test_conv, fold, scalar)
        
        # third train the model
        proc = subprocess.Popen(
            [self.caffe_root+"/build/tools/caffe","train",\
                "--solver="+self.caffe_root+self.caffe_example+eeg_conv_solver+".prototxt",\
                "--weights="+self.caffe_root+self.caffe_example+caffe_model+".caffemodel"], 
            stderr=subprocess.PIPE)
        res = proc.communicate()[1]
        
        return res
    
    def train_one_h_model(self, eeg_conv_solver, eeg_train_test_conv):       
        # third train the model
        proc = subprocess.Popen(
            [self.caffe_root+"/build/tools/caffe","train",\
                "--solver="+self.caffe_root+self.caffe_example+eeg_conv_solver+".prototxt"], 
            stderr=subprocess.PIPE)
        res = proc.communicate()[1]    
        
        return res
    
    def train_baseline_model(self, eeg_conv_solver, eeg_train_test_conv, fold, scalar, prefix='_lmdb_'):
        # then training
        # first editing prototxt
        self.edit_prototxt(eeg_conv_solver, eeg_train_test_conv, fold)
                           
        # second editing conv prototxt
        self.edit_model(eeg_train_test_conv, fold, scalar, prefix)
          
        # third train the model
        proc = subprocess.Popen(
            [self.caffe_root+"/build/tools/caffe","train",\
                "--solver="+self.caffe_root+self.caffe_example+eeg_conv_solver+".prototxt"], 
            stderr=subprocess.PIPE)
        res = proc.communicate()[1]
        
        return res    
    
    def train_one_model(self, eeg_conv_solver, eeg_train_test_conv, fold):
        # then training
        # first editing prototxt
        self.edit_prototxt(eeg_conv_solver, eeg_train_test_conv, fold)
                           
#        # second editing conv prototxt
#        self.edit_model(eeg_train_test_conv, prefix, subID, fold, scalar)
          
        # third train the model
        proc = subprocess.Popen(
            [self.caffe_root+"/build/tools/caffe","train",\
                "--solver="+self.caffe_root+self.caffe_example+eeg_conv_solver+".prototxt"], 
            stderr=subprocess.PIPE)
        res = proc.communicate()[1]
        
        return res
    
    def convert2feat(self, net, testInputs, testLabels, topLayer):
        input_shape = testInputs.shape
        input_step = 1 # int(np.floor(input_shape[0]/3))
        # blob_shape = (0,) + input_shape[1:len(input_shape)]
        blob_shape = (0, 100)
        x_train = np.array([],dtype=np.float64).reshape(blob_shape)
        y_train = []
        for in_idx in range(0, input_shape[0]):
            in_inputs = testInputs[in_idx*input_step:(in_idx+1)*input_step, :, :, :]
            in_labels = testLabels[in_idx*input_step:(in_idx+1)*input_step]
            out = net.forward(data=in_inputs)
        
            output_layer = net.blobs[topLayer].data
            # output_layer = out['prob']        
            
            x_train = np.concatenate((x_train, output_layer), axis=0)
            y_train = np.append(y_train, in_labels)
        return x_train, y_train    
    
    def convert2blob(self, net, testInputs, testLabels):
        input_shape = testInputs.shape
        input_step = 1 # int(np.floor(input_shape[0]/3))
        input_size = len(np.unique(testLabels))
        x_train = np.array([],dtype=np.float64).reshape(0, input_size)
        y_train = []
        for in_idx in range(0, input_shape[0]):
            in_inputs = testInputs[in_idx*input_step:(in_idx+1)*input_step, :, :, :]
            in_labels = testLabels[in_idx*input_step:(in_idx+1)*input_step]
            out = net.forward(data=in_inputs)
        
            output_layer = out['prob']        
            
            x_train = np.concatenate((x_train, output_layer), axis=0)
            y_train = np.append(y_train, in_labels)
        return x_train, y_train
    
    def get_accuracy(self, train_x, test_x, train_y, test_y, \
                            eeg_prod, fold, scalar, iterations, flag='auc'):
        import caffe
        MODEL_FILE = self.caffe_root+self.caffe_example+eeg_prod+'.prototxt'
        PRETRAINED = self.caffe_root+self.caffe_example+self.module_type+str(fold)+'_iter_'+\
                        str(iterations)+'.caffemodel'
        # train the model
        net = caffe.Net(MODEL_FILE, PRETRAINED, caffe.TEST)
        # read test data
        test_x = test_x*scalar
        x_test, y_test = self.convert2blob(net, test_x, test_y)
        # get result
        if flag == 'auc':
            fpr, tpr, thresholds = metrics.roc_curve(y_test, x_test[:,1], pos_label=1)
            auc = metrics.auc(fpr,tpr)
            return auc
        else:
            x_test = np.argmax(x_test, axis = 1)
            x_test = x_test + 1
            y_test = y_test.astype(int)
            acc = 1.*np.sum(x_test==y_test)/len(y_test)
            return acc

    def get_feature(self, train_x, test_x, train_y, test_y, \
                            eeg_prod, fold, scalar, iterations, topLayer='l1'):
        import caffe
        MODEL_FILE = self.caffe_root+self.caffe_example+eeg_prod+'.prototxt'
        PRETRAINED = self.caffe_root+self.caffe_example+self.module_type+str(fold)+'_iter_'+\
                        str(iterations)+'.caffemodel'
        # train the model
        net = caffe.Net(MODEL_FILE, PRETRAINED, caffe.TEST)
        # read train and test data
        train_x = train_x*scalar        
        test_x = test_x*scalar  
        # convert to features
        x_train, y_train    = self.convert2feat(net, train_x, train_y, topLayer)
        x_test, y_test      = self.convert2feat(net, test_x, test_y, topLayer)
        
        return x_train, y_train, x_test, y_test
        