# -*- coding: utf-8 -*-
"""
Created on Sun Aug 16 18:53:37 2015

@author: zijing
"""
import scipy
import h5py
import numpy as np

# Make sure that caffe is on the python path:
caffe_root = '/home/zijing/caffe/'  # this file is expected to be in {caffe_root}/examples
caffe_example= 'examples/DLAutoTest/'
import sys
sys.path.insert(0, caffe_root + 'python')
sys.path.append(caffe_root)
from examples.DLAutoTest import mat2lmdb
from examples.DLAutoTest import transferModule
from examples.DLAutoTest import randomSearchGenerator
        
        
'''
This training process is mainly for cross-subject test, each slot of variables
should store data from only one subject

dataIdx:    different feature types/datasets that used for classification

fold:       folds for separating epochs into training/testing

idx:        different model that can be used for training/validation

data:       set your data file use the format 'P{dataIdx}.mat'
            two variables are contained:
                Inputs
                Labels
            
Inputs:     the variable contains multiple cells, each cell is one subject
            mat cell struct: {1 * SubjectSize}
Labels:     the variable contains multiple cells, each cell is one subject
            mat cell struct: {1 * SubjectSize}
'''

maxDataIdx = 1
maxFold = 10
maxRandomSearchNum = 64
for dataIdx in range(0, maxDataIdx):   
    # load data first
    data = '/home/zijing/Downloads/P'+str(dataIdx)+'.mat'
#    mat = scipy.io.loadmat(data)    
#    Inputs     =   mat['Inputs'][0]
#    Labels     =   mat['Labels'][0]
    
    Inputs = []
    Labels = []
    with h5py.File(data) as f:
        for element in f['Inputs'][0]:
            print(element)
            Inputs.append(f[element][:])
        for element in f['Labels'][0]:    
            Labels.append(f[element][:][:, 0])
            
    blob_dim = list(Inputs[0].shape)
    # check the blob dimension with [featureMaps * channels * timeSamples]
    blob_dim.pop(0)
    
    ###########################################################################
    print('=' * 78)
    print('Current feature choose: ' + str(dataIdx))
    print('DNN as model')
    print('=' * 78)
    # get the random list to construct .prototxt
    iterations = 5000
    if dataIdx == 0:
        scalar = 0.0001
    else:
        scalar = 1
    maxLayerNum = 4
    topLayer = 2
    middleLayer = [100*2**j for j in reversed(range(0,maxLayerNum))]
    dropLayer = 0.5
    randLayer_str_list, randLayer_list=randomSearchGenerator.genRandomSearchList \
            (maxLayerNum, topLayer, middleLayer, dropLayer, maxRandomSearchNum)
    transferMod = transferModule.transferModule(caffe_root, caffe_example, 'mlp')        
    transferMod.edit_prototxt_iter('rand_solver', iterations)        
        
    '''
    Do a random search for the DNN
    set fold = 0 as the validation for model selection
    '''
    valid_fold = 0    
    
    # write to the lmdb file
    train_x, test_x, train_y, test_y = \
            mat2lmdb.generate_one_fold_lmdb(Inputs, Labels, valid_fold)
    
    auc_model_selection = np.zeros([maxRandomSearchNum, 20]) 
    for idx in range(0, maxRandomSearchNum):
        # first revise the training and testing prototxt
        randomSearchGenerator.constructPrototxt(randLayer_list[idx], \
                            'rand_train', 'rand_test', valid_fold, scalar, blob_dim)
        # then do training
        res = transferMod.train_one_model('rand_solver', 'rand_train', valid_fold)
        # then do testing
        # the result stored here
        auc = np.zeros([20])
        for i in range(1,21):
            auc[i-1]    =   transferMod.get_accuracy\
                            (train_x, test_x, train_y, test_y, \
                            'rand_test', valid_fold, scalar, iterations*i, 'auc')   
        auc_model_selection[idx] = auc  
                 
    # after train models, get the best model and iterations
    curr_model,curr_iter = \
        np.unravel_index(auc_model_selection.argmax(), auc_model_selection.shape)
    curr_iter = curr_iter+1
    print('We have found the best model is: ')
    print('=' * 78)
    print(randLayer_str_list[curr_model])    
    print('=' * 78)
    print('The iteration we use is: '+str((curr_iter+1)*iterations))
    print('=' * 78)
    
    auc_all = np.zeros([maxFold, 20]) 
    for fold in range(0, maxFold):    
        '''
        Do a test for the DNN
        '''
        train_x, test_x, train_y, test_y = \
            mat2lmdb.generate_one_fold_lmdb(Inputs, Labels, fold)
            
        randomSearchGenerator.constructPrototxt(randLayer_list[curr_model], \
                            'rand_train', 'rand_test', fold, scalar, blob_dim)
        # then do training
        transferMod.train_one_model('rand_solver', 'rand_train', fold)
        # then do testing
        # the result stored here
        auc = np.zeros([20])     
        for i in range(1,21):
            auc[i-1]    =   transferMod.get_accuracy\
                            (train_x, test_x, train_y, test_y, \
                            'rand_test', fold, scalar, iterations*i, 'auc')   

        # auc_dnn_CS.append(randLayer_str_list[idx])
        # store all aucs
        auc_all[fold] = auc          
    
    mod_all = randLayer_list[curr_model]    
    ite_all = iterations*curr_iter
    # write to mat file
    scipy.io.savemat(caffe_root+caffe_example+'dnn'+str(dataIdx)+'.mat', \
                    {'auc': auc_all, 'model': mod_all, 'iters': ite_all})

    print('=' * 78)   
    ########################################################################### 
    print('\n' * 2)
    ###########################################################################
    print('=' * 78)
    print('Current feature choose: ' + str(dataIdx))
    print('CNN-2Layer as model')
    print('=' * 78)    
    
    iterations = 5000
    if dataIdx == 0:
        scalar = 0.001
    else:
        scalar = 1
    if dataIdx == 0:
        timefilter1 = [2**j for j in reversed(range(0,5))]
    else:
        timefilter1 = [2**j for j in reversed(range(0,3))]
    timefilter2 = [2**j for j in reversed(range(0,5))]
    filterNum = [5*j for j in reversed(range(1,6))]
    maxLayerNum = 3
    topLayer = 2
    middleLayer = [100*2**j for j in reversed(range(0,maxLayerNum))]
    dropLayer = 0.5
    randLayer_str_list, randLayer_list=randomSearchGenerator.genRandomSearchListCNN\
                            (timefilter1, timefilter2, filterNum, maxLayerNum, \
                            topLayer, middleLayer, dropLayer, maxRandomSearchNum, blob_dim)    
    # initialize transfer module for cnn
    transferMod = transferModule.transferModule(caffe_root, caffe_example, 'cnn')
    transferMod.edit_prototxt_iter('rand_solver', iterations)  
    
    # still use the previous inputs and labels
    '''
    Do a random search for the CNN-2Layer
    set fold = 0 as the validation for model selection
    '''
    valid_fold = 0   
    # write to the lmdb file
    train_x, test_x, train_y, test_y = \
            mat2lmdb.generate_one_fold_lmdb(Inputs, Labels, valid_fold)
            
    auc_model_selection = np.zeros([maxRandomSearchNum, 20]) 
    for idx in range(0, maxRandomSearchNum):
        # first revise the training and testing prototxt
        randomSearchGenerator.constructPrototxtCNN(randLayer_list[idx], \
                            'rand_train', 'rand_test', valid_fold, scalar, blob_dim)
        # then do training
        res = transferMod.train_one_model('rand_solver', 'rand_train', valid_fold)
        # then do testing
        # the result stored here
        auc = np.zeros([20])
        for i in range(1,21):
            auc[i-1]    =   transferMod.get_accuracy\
                            (train_x, test_x, train_y, test_y, \
                            'rand_test', valid_fold, scalar, iterations*i, 'auc')   
        auc_model_selection[idx] = auc  
    # after train models, get the best model and iterations
    curr_model,curr_iter = \
        np.unravel_index(auc_model_selection.argmax(), auc_model_selection.shape)
        
    print('We have found the best model is: ')
    print('=' * 78)
    print(randLayer_str_list[curr_model])    
    print('=' * 78)
    print('The iteration we use is: '+str((curr_iter+1)*iterations))
    print('=' * 78)
    
    auc_all = np.zeros([maxFold, 20]) 
    for fold in range(0, maxFold):    
        '''
        Do a test for the CNN
        '''
        train_x, test_x, train_y, test_y = \
            mat2lmdb.generate_one_fold_lmdb(Inputs, Labels, fold)
            
        randomSearchGenerator.constructPrototxtCNN(randLayer_list[curr_model], \
                            'rand_train', 'rand_test', fold, scalar, blob_dim)
        # then do training
        transferMod.train_one_model('rand_solver', 'rand_train', fold)
        # then do testing
        # the result stored here
        auc = np.zeros([20])     
        for i in range(1,21):
            auc[i-1]    =   transferMod.get_accuracy\
                            (train_x, test_x, train_y, test_y, \
                            'rand_test', fold, scalar, iterations*i, 'auc')   

        # auc_dnn_CS.append(randLayer_str_list[idx])
        # store all aucs
        auc_all[fold] = auc          
    
    mod_all = randLayer_list[curr_model]    
    ite_all = iterations*curr_iter
    # write to mat file
    scipy.io.savemat(caffe_root+caffe_example+'cnn2L'+str(dataIdx)+'.mat', \
                    {'auc': auc_all, 'model': mod_all, 'iters': ite_all})

    
    print('=' * 78)
    ###########################################################################
    print('\n' * 2)
    ###########################################################################
    print('=' * 78)
    print('Current feature choose: ' + str(dataIdx))
    print('CNN-1Layer as model')
    print('=' * 78)    
    
    iterations = 5000
    if dataIdx == 0:
        scalar = 0.001
    else:
        scalar = 1
    if dataIdx == 0:
        timefilter1 = [2**j for j in reversed(range(0,5))]
    else:
        timefilter1 = [2**j for j in reversed(range(0,5))]    
    timefilter2 = []
    filterNum = [5*j for j in reversed(range(1,6))]
    maxLayerNum = 3
    topLayer = 2
    middleLayer = [100*2**j for j in reversed(range(0,maxLayerNum))]
    dropLayer = 0.5
    randLayer_str_list, randLayer_list=randomSearchGenerator.genRandomSearchListCNN\
                            (timefilter1, timefilter2, filterNum, maxLayerNum, \
                            topLayer, middleLayer, dropLayer, maxRandomSearchNum, blob_dim)    
    # initialize transfer module for cnn
    transferMod = transferModule.transferModule(caffe_root, caffe_example, 'cnn')
    transferMod.edit_prototxt_iter('rand_solver', iterations)  
    
    # still use the previous inputs and labels
    '''
    Do a random search for the CNN-1Layer
    set fold = 0 as the validation for model selection
    '''
    valid_fold = 0   
    # write to the lmdb file
    train_x, test_x, train_y, test_y = \
            mat2lmdb.generate_one_fold_lmdb(Inputs, Labels, valid_fold)
            
    auc_model_selection = np.zeros([maxRandomSearchNum, 20]) 
    for idx in range(0, maxRandomSearchNum):
        # print(str(idx))
        # first revise the training and testing prototxt
        randomSearchGenerator.constructPrototxtCNN(randLayer_list[idx], \
                            'rand_train', 'rand_test', valid_fold, scalar, blob_dim)
        # then do training
        res = transferMod.train_one_model('rand_solver', 'rand_train', valid_fold)
        # then do testing
        # the result stored here
        auc = np.zeros([20])
        for i in range(1,21):
            auc[i-1]    =   transferMod.get_accuracy\
                            (train_x, test_x, train_y, test_y, \
                            'rand_test', valid_fold, scalar, iterations*i, 'auc')   
        auc_model_selection[idx] = auc  
    # after train models, get the best model and iterations
    curr_model,curr_iter = \
        np.unravel_index(auc_model_selection.argmax(), auc_model_selection.shape)
        
    print('We have found the best model is: ')
    print('=' * 78)
    print(randLayer_str_list[curr_model])    
    print('=' * 78)
    print('The iteration we use is: '+str((curr_iter+1)*iterations))
    print('=' * 78)
    
    auc_all = np.zeros([maxFold, 20]) 
    for fold in range(0, maxFold):    
        '''
        Do a test for the CNN
        '''
        train_x, test_x, train_y, test_y = \
            mat2lmdb.generate_one_fold_lmdb(Inputs, Labels, fold)
            
        randomSearchGenerator.constructPrototxtCNN(randLayer_list[curr_model], \
                            'rand_train', 'rand_test', fold, scalar, blob_dim)
        # then do training
        transferMod.train_one_model('rand_solver', 'rand_train', fold)
        # then do testing
        # the result stored here
        auc = np.zeros([20])     
        for i in range(1,21):
            auc[i-1]    =   transferMod.get_accuracy\
                            (train_x, test_x, train_y, test_y, \
                            'rand_test', fold, scalar, iterations*i, 'auc')   

        # auc_dnn_CS.append(randLayer_str_list[idx])
        # store all aucs
        auc_all[fold] = auc          
    
    mod_all = randLayer_list[curr_model]    
    ite_all = iterations*curr_iter
    # write to mat file
    scipy.io.savemat(caffe_root+caffe_example+'cnn1L'+str(dataIdx)+'.mat', \
                    {'auc': auc_all, 'model': mod_all, 'iters': ite_all})
                    
                    
    print('=' * 78)
    ###########################################################################
