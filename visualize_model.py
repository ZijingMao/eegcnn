# -*- coding: utf-8 -*-
"""
Created on Sun May 10 20:46:03 2015

@author: zijing
"""

'''
Given an model, visualize the model to verify if model is no problem
'''

caffe_root = '/home/zijing/caffe/'  # this file is expected to be in {caffe_root}/examples
import sys
sys.path.insert(0, caffe_root + 'python')

from google.protobuf import text_format
from caffe.draw import get_pydot_graph
from caffe.proto import caffe_pb2
from IPython.display import display, Image 

MODEL_FILE = caffe_root+'examples/DLAutoTest/googleNetTrain.prototxt'

_net = caffe_pb2.NetParameter()
f = open(MODEL_FILE)
text_format.Merge(f.read(), _net)
display(Image(get_pydot_graph(_net,"TB").create_png()))
