# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 15:04:31 2015

@author: zijing
"""
import scipy
import h5py
import numpy as np

# Make sure that caffe is on the python path:
caffe_root = '/home/zijing/caffe/'  # this file is expected to be in {caffe_root}/examples
caffe_example= 'examples/DLAutoTest/'
import sys
sys.path.insert(0, caffe_root + 'python')
sys.path.append(caffe_root)
from examples.DLAutoTest import mat2lmdb
from examples.DLAutoTest import transferModule
from examples.DLAutoTest import randomSearchGenerator

'''
These tests are based on models proposed by:

paper [1]:  Convolutional Neural Networks for P300 Detection with Application to
            Brain-Computer Interfaces 
paper [2]:  Time Series Classification Using Multi-Channels Deep Convolutional 
            Neural Networks
'''  

maxDataIdx = 1
maxFold = 10
for dataIdx in range(0, maxDataIdx):   
    # load data first
    data = '/home/zijing/Downloads/P'+str(dataIdx)+'.mat'
#    mat = scipy.io.loadmat(data)    
#    Inputs     =   mat['Inputs'][0]
#    Labels     =   mat['Labels'][0]
    
    f = h5py.File(data)
    Inputs = f['Inputs'][:]
    Labels = f['Labels'][:]    
    
    blob_dim = list(Inputs[0].shape)
    # check the blob dimension with [featureMaps * channels * timeSamples]
    blob_dim.pop(0)
    
    ###########################################################################
    print('=' * 78)
    print('Current feature choose: ' + str(dataIdx))
    print('CNN-1SF1TF as model')
    print('=' * 78)    
    
    iterations = 500
    if dataIdx == 0:
        scalar = 0.001
    else:
        scalar = 1
    transferMod = transferModule.transferModule(caffe_root, caffe_example, 'mlp')    
    transferMod.edit_prototxt_iter('rand_solver_1sf_1tf', iterations)    

    ''' modify model in file:
        rand_solver_1sf_1tf.prototxt
        rand_test_1sf_1tf.prototxt
        rand_train_1sf_1tf.prototxt
    '''
    auc_all = np.zeros([maxFold, 20]) 
    for fold in range(0, maxFold): 
        train_x, test_x, train_y, test_y = \
            mat2lmdb.generate_one_fold_lmdb(Inputs, Labels, fold)
            
        transferMod.train_baseline_model('rand_solver_1sf_1tf', \
                                         'rand_train_1sf_1tf', fold, scalar)
        auc = np.zeros([20])
        transferMod.edit_model_test('rand_test_1sf_1tf', blob_dim)
        for i in range(1,21):
            auc[i-1]    =   transferMod.get_accuracy\
                            (train_x, test_x, train_y, test_y, \
                            'rand_test', fold, scalar, iterations*i, 'auc')   
        # store all aucs
        auc_all[fold] = auc 
        
    # write to mat file
    scipy.io.savemat(caffe_root+caffe_example+'cnn1sf_1tf'+str(dataIdx)+'.mat', \
                    {'auc': auc_all})

    print('=' * 78)   
    ########################################################################### 
    print('\n' * 2)
    ###########################################################################
    print('=' * 78)
    print('Current feature choose: ' + str(dataIdx))
    print('CNN-2TF as model')
    print('=' * 78)    
    
    iterations = 500
    if dataIdx == 0:
        scalar = 0.001
    else:
        scalar = 1
    transferMod = transferModule.transferModule(caffe_root, caffe_example, 'mlp')    
    transferMod.edit_prototxt_iter('rand_solver_2tf', iterations)    

    ''' modify model in file:
        rand_solver_2tf.prototxt
        rand_test_2tf.prototxt
        rand_train_2tf.prototxt
    '''
    auc_all = np.zeros([maxFold, 20]) 
    for fold in range(0, maxFold): 
        train_x, test_x, train_y, test_y = \
            mat2lmdb.generate_one_fold_lmdb(Inputs, Labels, fold)
            
        transferMod.train_baseline_model('rand_solver_2tf', \
                                         'rand_train_2tf', fold, scalar)
        auc = np.zeros([20])
        transferMod.edit_model_test('rand_test_2tf', blob_dim)
        for i in range(1,21):
            auc[i-1]    =   transferMod.get_accuracy\
                            (train_x, test_x, train_y, test_y, \
                            'rand_test', fold, scalar, iterations*i, 'auc')   
        # store all aucs
        auc_all[fold] = auc 
        
    # write to mat file
    scipy.io.savemat(caffe_root+caffe_example+'cnn2tf'+str(dataIdx)+'.mat', \
                    {'auc': auc_all})
                    
    print('=' * 78)   
    ###########################################################################
    print('\n' * 2)
    ###########################################################################
    print('=' * 78)
    print('Current feature choose: ' + str(dataIdx))
    print('CNN-1SF as model')
    print('=' * 78)    
    
    iterations = 500
    if dataIdx == 0:
        scalar = 0.001
    else:
        scalar = 1
    if dataIdx == 0:
        timefilter1 = [1]
    else:
        timefilter1 = [1]    
    timefilter2 = []
    filterNum = [4]
    maxLayerNum = 1
    topLayer = 2
    middleLayer = [100]
    dropLayer = 0.5
    randLayer_str_list, randLayer_list=randomSearchGenerator.genRandomSearchListCNN\
                            (timefilter1, timefilter2, filterNum, maxLayerNum, \
                            topLayer, middleLayer, dropLayer, 1, blob_dim)    
    # initialize transfer module for cnn
    transferMod = transferModule.transferModule(caffe_root, caffe_example, 'cnn')
    transferMod.edit_prototxt_iter('rand_solver', iterations)  
    
    # still use the previous inputs and labels
    '''
    Do a random search for the CNN-1Layer
    set fold = 0 as the validation for model selection
    '''
    valid_fold = 0   
    # write to the lmdb file
    train_x, test_x, train_y, test_y = \
            mat2lmdb.generate_one_fold_lmdb(Inputs, Labels, valid_fold)
            
    auc_model_selection = np.zeros([1, 20]) 
    for idx in range(0, 1):
        # print(str(idx))
        # first revise the training and testing prototxt
        randomSearchGenerator.constructPrototxtCNN(randLayer_list[idx], \
                            'rand_train', 'rand_test', valid_fold, scalar, blob_dim)
        # then do training
        res = transferMod.train_one_model('rand_solver', 'rand_train', valid_fold)
        # then do testing
        # the result stored here
        auc = np.zeros([20])
        for i in range(1,21):
            auc[i-1]    =   transferMod.get_accuracy\
                            (train_x, test_x, train_y, test_y, \
                            'rand_test', valid_fold, scalar, iterations*i, 'auc')   
        auc_model_selection[idx] = auc  
    # after train models, get the best model and iterations
    curr_model,curr_iter = \
        np.unravel_index(auc_model_selection.argmax(), auc_model_selection.shape)
        
    print('We have found the best model is: ')
    print('=' * 78)
    print(randLayer_str_list[curr_model])    
    print('=' * 78)
    print('The iteration we use is: '+str((curr_iter+1)*iterations))
    print('=' * 78)
    
    auc_all = np.zeros([maxFold, 20]) 
    for fold in range(0, maxFold):    
        '''
        Do a test for the CNN
        '''
        train_x, test_x, train_y, test_y = \
            mat2lmdb.generate_one_fold_lmdb(Inputs, Labels, fold)
            
        randomSearchGenerator.constructPrototxtCNN(randLayer_list[curr_model], \
                            'rand_train', 'rand_test', fold, scalar, blob_dim)
        # then do training
        transferMod.train_one_model('rand_solver', 'rand_train', fold)
        # then do testing
        # the result stored here
        auc = np.zeros([20])     
        for i in range(1,21):
            auc[i-1]    =   transferMod.get_accuracy\
                            (train_x, test_x, train_y, test_y, \
                            'rand_test', fold, scalar, iterations*i, 'auc')   

        # auc_dnn_CS.append(randLayer_str_list[idx])
        # store all aucs
        auc_all[fold] = auc          
    
    mod_all = randLayer_list[curr_model]    
    ite_all = iterations*curr_iter
    # write to mat file
    scipy.io.savemat(caffe_root+caffe_example+'cnn1sf'+str(dataIdx)+'.mat', \
                    {'auc': auc_all, 'model': mod_all, 'iters': ite_all})
                    
                    
    print('=' * 78)
    ###########################################################################              
