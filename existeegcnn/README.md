## existeegcnn: *Existing CNN for EEG models*

We used 3 models to test EEG.

CNN-1SF1TF

CNN-2TF

CNN-1SF

### Introduction

We also want to test if existing eeg cnn models.

### Modify existeegcnn for eeg signals

0. **Prerequisites** 
  0. Caffe's [prerequisites](http://caffe.berkeleyvision.org/installation.html#prequequisites)
0. **Install Caffe** (this is the most complicated part)
  0. Download [Caffe v0.999](https://github.com/BVLC/caffe/archive/v0.999.tar.gz)
  0. Follow the [Caffe installation instructions](http://caffe.berkeleyvision.org/installation.html)
0. **Modify model config**
  0. change bash file .sh with '--solver=/correct/directory/Solver.prototxt'
  0. change solver.prototxt with correct directory
  0. change Train.prototxt with correct directory
  0. change train_baseline_model.py with correct directory

### Running existeegcnn on eeg dataset

Let's assume you have set every configuration and directory correct, now:

1. Choose one bash file to execute: `./your/directory/yourNet.sh` as verification of no issue for prototxt
2. Run the batch code for mutilple eeg dataset: `runfile('train_baseline_models.py')`

**Note:** Most of the time an error happens is due to the directory is not set correctly

