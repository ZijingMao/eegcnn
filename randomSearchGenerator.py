# -*- coding: utf-8 -*-
"""
Created on Mon Aug 31 16:39:38 2015

@author: zijing
"""

import numpy as np

caffe_root = '/home/zijing/caffe/'  # this file is expected to be in {caffe_root}/examples
caffe_example= 'examples/DLAutoTest/'

## for HCNN there is no maxSplitLayerNum because we only use two conv layers
def genRandomSearchListHCNN(timefilter1, timefilter2, filterNum, maxLayerNum, \
                        topLayer, middleLayer, dropLayer, maxRandomSearchNum, windowNum, blob_dim):
    randLayer_str_list = []
    randLayer_list = []
    times = 0
    while times < maxRandomSearchNum:
        randLayer = []
        currentLayer = []
        currentElement = []
        # first generate two convolution layer
        # the first convolution layer is a filter pass all channels
        currentElement.append(blob_dim[1])
        idx1 = np.random.randint(0, len(timefilter1))
        # then it will choose the time length to pass
        currentElement.append(timefilter1[idx1])
        idxFilter = np.random.randint(0, len(filterNum))
        # last it will append the size of feature maps
        currentElement.append(filterNum[idxFilter])
        # append current element to current layer
        for win in range(0, windowNum):
            currentLayer.append(currentElement)
        # then append current layer to randLayer
        randLayer.append(currentLayer)
        
        currentLayer = []
        currentElement = []
        # the following layer is the pooling layer
        currentElement = [1, 2] # indication the pooling size
        # append current element to current layer
        for win in range(0, windowNum):
            currentLayer.append(currentElement)
        randLayer.append(currentLayer)
        
        # the following layer is the second convolution layer
        # it is a layer pass just one channel but across time
        currentLayer = []
        currentElement = []
        # append 1 because there is only 1 dimension in the 3rd dimension
        currentElement.append(1)    
        idx2 = np.random.randint(0, len(timefilter2))
        # then it will choose the time length to pass
        currentElement.append(timefilter2[idx2])
        idxFilter = np.random.randint(0, len(filterNum))
        # last it will append the size of feature maps
        currentElement.append(filterNum[idxFilter])
        # append current element to current layer
        for win in range(0, windowNum):
            currentLayer.append(currentElement)
        # then append current layer to randLayer
        randLayer.append(currentLayer)
        
        currentLayer = []
        currentElement = []
        # the following layer is the pooling layer
        currentElement = [1, 2] # indication the pooling size
        # append current element to current layer
        for win in range(0, windowNum):
            currentLayer.append(currentElement)
        randLayer.append(currentLayer)
        
        # from top to bot layer, define the size
        for i in range(0, maxLayerNum):
            # add drop layer (drop layer only add on bot or top)
            if i == 0 and np.random.randint(0, 3):  # the probability to get a dropout layer
                randLayer.append([dropLayer])
            layerFlag = np.random.randint(0, 3)
            if layerFlag == 0:
                # indicate not use this layer
                # do nothing
                pass
            elif layerFlag == 2 and i != maxLayerNum-1:
                # indicate use the current idx for layer
                randLayer.append([middleLayer[i+1]])
            else:
                # indicate use the next idx for layer
                randLayer.append([middleLayer[i]])
            if i == maxLayerNum-1 and np.random.randint(0, 3):
                randLayer.append([dropLayer])
        # append the top layer
        randLayer.append([topLayer])
        # concat to string and make it unique
        randLayer_str = ""
        for randSameLayer in randLayer:
            for i in randSameLayer:
                randLayer_str += str(i) + "*"
            # remove the last appendix '=' and add new appendix '-'
            randLayer_str = randLayer_str[:-1]
            randLayer_str += "-"
        # remove the last appendix '-'
        randLayer_str = randLayer_str[:-1]
        # put in the list
        if randLayer_str not in randLayer_str_list:
            randLayer_str_list.append(randLayer_str)
            randLayer_list.append(randLayer)
            times = times + 1
            
    return randLayer_str_list, randLayer_list

def genRandomSearchListH(maxSplitLayerNum, maxFullLayerNum, topLayer, \
                        middleSplitLayer, middleFullLayer, dropLayer, \
                        maxRandomSearchNum, windowNum):
    randLayer_str_list = []
    randLayer_list = []
    times = 0
    while times < maxRandomSearchNum:
        randLayer = []
        # from top to bot layer, define the size
        # top should be the split with window Number
        for i in range(0, maxSplitLayerNum):
            if i == 0 and np.random.randint(0, 2):
                randLayer.append([dropLayer]) # add the dropout layer at the bottom
            # define each layer
            layerFlag = np.random.randint(0, 3)
            randSameLayer = []  # it stores the number for the same layer
            if layerFlag == 0:
                # indicate not use this layer
                # do nothing
                pass
            elif layerFlag == 2 and i != maxSplitLayerNum-1:
                # indicate use the current idx for layer
                for win in range(0, windowNum): # append the layer for each window
                    randSameLayer.append(middleSplitLayer[i+1])
            else:
                # indicate use the next idx for layer
                for win in range(0, windowNum): # append the layer for each window
                    randSameLayer.append(middleSplitLayer[i])
            # append everything in randSameLayer  if it is not empty
            if randSameLayer:
                randLayer.append(randSameLayer)
                
        for i in range(0, maxFullLayerNum):
            layerFlag = np.random.randint(0, 3)
            if layerFlag == 0:
                # indicate not use this layer
                # do nothing
                pass
            elif layerFlag == 2 and i != maxFullLayerNum-1:
                # indicate use the current idx for layer
                randLayer.append([middleFullLayer[i+1]])
            else:
                # indicate use the next idx for layer
                randLayer.append([middleFullLayer[i]])
            if i == maxFullLayerNum-1 and np.random.randint(0, 2):
                randLayer.append([dropLayer])
            
        # append the top layer
        randLayer.append([topLayer])     
        # concat to string and make it unique
        randLayer_str = ""
        for randSameLayer in randLayer:
            for i in randSameLayer:
                randLayer_str += str(i) + "="
            # remove the last appendix '=' and add new appendix '-'
            randLayer_str = randLayer_str[:-1]
            randLayer_str += "-"
        # remove the last appendix '-'
        randLayer_str = randLayer_str[:-1]
        # put in the list
        if randLayer_str not in randLayer_str_list:
            randLayer_str_list.append(randLayer_str)
            randLayer_list.append(randLayer)
            times = times + 1

    return randLayer_str_list, randLayer_list

def genRandomSearchListCNN(timefilter1, timefilter2, filterNum, maxLayerNum, \
                        topLayer, middleLayer, dropLayer, maxRandomSearchNum, blob_dim):
    randLayer_str_list = []
    randLayer_list = []
    times = 0
    while times < maxRandomSearchNum:
        randLayer = []
        currentLayer = []
        # first generate two convolution layer
        # the first convolution layer is a filter pass all channels
        currentLayer.append(blob_dim[1])    
        idx1 = np.random.randint(0, len(timefilter1))
        # then it will choose the time length to pass
        currentLayer.append(timefilter1[idx1])
        idxFilter = np.random.randint(0, len(filterNum))
        # last it will append the size of feature maps
        currentLayer.append(filterNum[idxFilter])
        # then append current layer to randLayer
        randLayer.append(currentLayer)
        # the following layer is the pooling layer
        currentLayer = [1, 2] # indication the pooling size
        randLayer.append(currentLayer)
        # the following layer is the second convolution layer
        # it is a layer pass just one channel but across time
        currentLayer = []
        # append 1 because there is only 1 dimension in the 3rd dimension
        currentLayer.append(1)    
        if len(timefilter2) > 0:
            idx2 = np.random.randint(0, len(timefilter2))
            # then it will choose the time length to pass
            currentLayer.append(timefilter2[idx2])
            idxFilter = np.random.randint(0, len(filterNum))
            # last it will append the size of feature maps
            currentLayer.append(filterNum[idxFilter])
            # then append current layer to randLayer
            randLayer.append(currentLayer)
            # the following layer is the pooling layer
            currentLayer = [1, 2] # indication the pooling size
            randLayer.append(currentLayer)
        # from top to bot layer, define the size
        for i in range(0, maxLayerNum):
            # add drop layer (drop layer only add on bot or top)
            if i == 0 and np.random.randint(0, 3):  # the probability to get a dropout layer
                randLayer.append([dropLayer])
            layerFlag = np.random.randint(0, 3)
            if layerFlag == 0:
                # indicate not use this layer
                # do nothing
                pass
            elif layerFlag == 2 and i != maxLayerNum-1:
                # indicate use the current idx for layer
                randLayer.append([middleLayer[i+1]])
            else:
                # indicate use the next idx for layer
                randLayer.append([middleLayer[i]])
            if i == maxLayerNum-1 and np.random.randint(0, 3):
                randLayer.append([dropLayer])
        # append the top layer
        randLayer.append([topLayer])
        # concat to string and make it unique
        randLayer_str = ""
        for randSameLayer in randLayer:
            for i in randSameLayer:
                randLayer_str += str(i) + "*"
            # remove the last appendix '=' and add new appendix '-'
            randLayer_str = randLayer_str[:-1]
            randLayer_str += "-"
        # remove the last appendix '-'
        randLayer_str = randLayer_str[:-1]
        # put in the list
        if randLayer_str not in randLayer_str_list:
            randLayer_str_list.append(randLayer_str)
            randLayer_list.append(randLayer)
            times = times + 1
            
    return randLayer_str_list, randLayer_list

def genRandomSearchList(maxLayerNum, topLayer, middleLayer, dropLayer, maxRandomSearchNum):
    randLayer_str_list = []
    randLayer_list = []
    times = 0
    while times < maxRandomSearchNum:
        randLayer = []
        # from top to bot layer, define the size
        for i in range(0, maxLayerNum):
            # add drop layer (drop layer only add on bot or top)
            if i == 0 and np.random.randint(0, 2):
                randLayer.append(dropLayer)
            layerFlag = np.random.randint(0, 3)
            if layerFlag == 0:
                # indicate not use this layer
                # do nothing
                pass
            elif layerFlag == 2 and i != maxLayerNum-1:
                # indicate use the current idx for layer
                randLayer.append(middleLayer[i+1])
            else:
                # indicate use the next idx for layer
                randLayer.append(middleLayer[i])
            if i == maxLayerNum-1 and np.random.randint(0, 2):
                randLayer.append(dropLayer)
        # append the top layer
        randLayer.append(topLayer)
        # concat to string and make it unique
        randLayer_str = ""
        for i in randLayer:
            randLayer_str += str(i) + "-"
        randLayer_str = randLayer_str[:-1]
        # put in the list
        if randLayer_str not in randLayer_str_list:
            randLayer_str_list.append(randLayer_str)
            randLayer_list.append(randLayer)
            times = times + 1
    return randLayer_str_list, randLayer_list

def constructPrototxtHCNN(randLayersAll, prototxt_name_train, prototxt_name_test, \
                      prefix, subID, fold, scalar):
                          
    # first clean the file
    with open(caffe_root+caffe_example+prototxt_name_train+".prototxt", "r+") as f:
        lines = f.readlines()
        f.seek(0)
        for line in lines:
            f.write('')
        f.truncate()
    f.close()
    with open(caffe_root+caffe_example+prototxt_name_test+".prototxt", "r+") as f:
        lines = f.readlines()
        f.seek(0)
        for line in lines:
            f.write('')
        f.truncate()
    f.close()
    
    # get the copy of the layer info
    randLayers = list(randLayersAll)
    # then revise the file
    with open(caffe_root+caffe_example+prototxt_name_train+".prototxt", "r+") as f:
        # write files
        f.seek(0)
        # write title
        f.write('name: "emo"\n') 
        # write the first layer - data_train layer
        name = 'ct'
        randLayer = 'data_train'
        bot = []
        top = ['data', 'label']
        include = ['phase: TRAIN']
        param1 = []
        param2 = []
        transform_param = ['scale: '+str(scalar)]
        data_param1 = 'source: "'+caffe_root+caffe_example+\
                            prefix+str(subID)+'train_lmdb_'+str(fold)+'"'
        data_param2 = 'batch_size: 32'
        data_param3 = 'backend: LMDB'
        data_param = [data_param1, data_param2, data_param3]
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)        
        # write the second layer - data_test layer
        name = 'ct'
        randLayer = 'data_test'
        bot = []
        top = ['data', 'label']
        include = ['phase: TEST']
        param1 = []
        param2 = []
        transform_param = ['scale: '+str(scalar)]
        data_param1 = 'source: "'+caffe_root+caffe_example+\
                            prefix+str(subID)+'test_lmdb_'+str(fold)+'"'
        data_param2 = 'batch_size: 32'
        data_param3 = 'backend: LMDB'
        data_param = [data_param1, data_param2, data_param3]
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)    
                    
        # then add the split layer
        # first get the window number
        winNum = len(randLayers[0])
        if winNum > 1: # if there are split layers
            randLayer = 'Slice'
            name = 'slice_pair'
            bot = ['data']
            top = []
            data_param = []
            data_param.append('axis: 1')   # add dimension for slice
            for i in range(0, winNum):
                top.append('data'+str(i))
                # add slice point each time
                if i+1 < winNum:
                    data_param.append('slice_point: '+str(i+1))
            include = []
            param1 = []
            param2 = []
            transform_param = []
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                            transform_param, data_param) 
        
        # write the fourth layer: convolution layer 1
        randLayer = randLayers[0]
        for n in range(0, winNum):
            element = randLayer[n]
            name = 'c'+str(n)+'0'
            bot = ['data'+str(n)]
            top = ['c'+str(n)+'0']
            include = []
            param1 = ['name: "c'+str(0)+'_w"', 'lr_mult: 1']
            param2 = ['name: "c'+str(0)+'_b"', 'lr_mult: 2']
            transform_param = []           
            data_param = []
            constructLayer(f, name, element, bot, top, include, param1, param2, \
                        transform_param, data_param) 
        randLayers.pop(0)   # pop the used layer
        randLayer = randLayers[0]
        for n in range(0, winNum):
            element = randLayer[n]
            # write the attached third layer: pooling layer 1       
            name = 'p'+str(n)+'0'
            bot = ['c'+str(n)+'0']
            top = ['p'+str(n)+'0']
            include = []
            param1 = []
            param2 = []
            transform_param = []           
            data_param = []
            constructLayer(f, name, element, bot, top, include, param1, param2, \
                        transform_param, data_param)        
        randLayers.pop(0)   # pop the used layer
        randLayer = randLayers[0]
        for n in range(0, winNum):
            element = randLayer[n]
            # write the fourth layer: convolution layer 2
            name = 'c'+str(n)+'1'
            bot = ['p'+str(n)+'0']
            top = ['c'+str(n)+'1']
            include = []
            param1 = ['name: "c'+str(1)+'_w"', 'lr_mult: 1']
            param2 = ['name: "c'+str(1)+'_b"', 'lr_mult: 2']
            transform_param = []           
            data_param = []
            constructLayer(f, name, element, bot, top, include, param1, param2, \
                        transform_param, data_param) 
        randLayers.pop(0)   # pop the used layer
        randLayer = randLayers[0]
        for n in range(0, winNum):
            element = randLayer[n]
            # write the attached fourth layer: pooling layer 2       
            name = 'p'+str(n)+'1'
            bot = ['c'+str(n)+'1']
            top = ['p'+str(n)+'1']
            include = []
            param1 = []
            param2 = []
            transform_param = []           
            data_param = []
            constructLayer(f, name, element, bot, top, include, param1, param2, \
                        transform_param, data_param)  
        randLayers.pop(0)   # pop the used layer
        
        # then the following layers are all fully connected layer
        # first add the concat layer:
        # get the merge layer
        ip_idx = 0
        if winNum > 1: # if there are split layers
            randLayer = 'Concat'
            name = 'concat'
            bot = []
            top = ['p1']
            data_param = []
            data_param.append('axis: 1')   # add dimension for concat
            for i in range(0, winNum):
                bot.append('p'+str(i)+str(1))   # append each data sections
            include = []
            param1 = []
            param2 = []
            transform_param = []
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                            transform_param, data_param)
        # for loop to write each layer
        for randLayer in randLayers:
            randLayer = randLayer[0]
            name = 'l'+str(ip_idx)
            if ip_idx == 0: # this means the layer is just up to data
                bot = ['p1']
            else:
                bot = ['l'+str(ip_idx-1)]
            top = ['l'+str(ip_idx)]
            include = []
            param1 = []
            param2 = []
            transform_param = []
            data_param = []
            if type(randLayer) == float:    
                pass # this means the layer is dropout and nothing need add
            elif type(randLayer) == int:    
                param1 = ['lr_mult: 1']
                param2 = ['lr_mult: 2']
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                        transform_param, data_param)    
            if ip_idx < len(randLayers)-1 and type(randLayer) == int: # if an ip layer
                # write relu layer
                f.write('layer {\n')  
                f.write('  name: "relu'+str(ip_idx)+'"\n')
                f.write('  type: "ReLU"\n')
                f.write('  bottom: "l'+str(ip_idx)+'"\n')
                f.write('  top: "l'+str(ip_idx)+'"\n')
                f.write('}\n')
            ip_idx = ip_idx+1
        # write the top layer - accuracy layer
        name = 'accuracy'
        randLayer = 'Accuracy'
        bot1 = 'l'+str(ip_idx-1)
        bot2 = 'label'
        bot = [bot1, bot2]
        top = ['accuracy']
        include = ['phase: TEST']
        param1 = []
        param2 = []
        transform_param = []
        data_param = []
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)   
        # write the top layer - loss layer
        name = 'loss'
        randLayer = 'SoftmaxWithLoss'
        bot1 = 'l'+str(ip_idx-1)
        bot2 = 'label'
        bot = [bot1, bot2]
        top = ['loss']
        include = []
        param1 = []
        param2 = []
        transform_param = []
        data_param = []
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)   
        
    # get the copy of the layer info
    randLayers = list(randLayersAll)
    # then revise the file
    with open(caffe_root+caffe_example+prototxt_name_test+".prototxt", "r+") as f:
        # write files
        f.seek(0)
        # write title
        f.write('name: "emo"\n') 
        # write the first layer - data_train layer
        f.write('input: "data"\n')  
        f.write('input_dim: 1\n')  
        f.write('input_dim: '+str(winNum)+'\n')  
        f.write('input_dim: blob_dim[1]\n')  
        f.write('input_dim: '+str(128/winNum)+'\n')  
        # then add the split layer
        # first get the window number
        winNum = len(randLayers[0])
        if winNum > 1: # if there are split layers
            randLayer = 'Slice'
            name = 'slice_pair'
            bot = ['data']
            top = []
            data_param = []
            data_param.append('axis: 1')   # add dimension for slice
            for i in range(0, winNum):
                top.append('data'+str(i))
                # add slice point each time
                if i+1 < winNum:
                    data_param.append('slice_point: '+str(i+1))
            include = []
            param1 = []
            param2 = []
            transform_param = []
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                            transform_param, data_param) 
        
        # write the fourth layer: convolution layer 1
        randLayer = randLayers[0]
        for n in range(0, winNum):
            element = randLayer[n]
            name = 'c'+str(n)+'0'
            bot = ['data'+str(n)]
            top = ['c'+str(n)+'0']
            include = []
            param1 = ['name: "c'+str(0)+'_w"', 'lr_mult: 1']
            param2 = ['name: "c'+str(0)+'_b"', 'lr_mult: 2']
            transform_param = []           
            data_param = []
            constructLayer(f, name, element, bot, top, include, param1, param2, \
                        transform_param, data_param) 
        randLayers.pop(0)   # pop the used layer
        randLayer = randLayers[0]
        for n in range(0, winNum):
            element = randLayer[n]
            # write the attached third layer: pooling layer 1       
            name = 'p'+str(n)+'0'
            bot = ['c'+str(n)+'0']
            top = ['p'+str(n)+'0']
            include = []
            param1 = []
            param2 = []
            transform_param = []           
            data_param = []
            constructLayer(f, name, element, bot, top, include, param1, param2, \
                        transform_param, data_param)        
        randLayers.pop(0)   # pop the used layer
        randLayer = randLayers[0]
        for n in range(0, winNum):
            element = randLayer[n]
            # write the fourth layer: convolution layer 2
            name = 'c'+str(n)+'1'
            bot = ['p'+str(n)+'0']
            top = ['c'+str(n)+'1']
            include = []
            param1 = ['name: "c'+str(1)+'_w"', 'lr_mult: 1']
            param2 = ['name: "c'+str(1)+'_b"', 'lr_mult: 2']
            transform_param = []           
            data_param = []
            constructLayer(f, name, element, bot, top, include, param1, param2, \
                        transform_param, data_param) 
        randLayers.pop(0)   # pop the used layer
        randLayer = randLayers[0]
        for n in range(0, winNum):
            element = randLayer[n]
            # write the attached fourth layer: pooling layer 2       
            name = 'p'+str(n)+'1'
            bot = ['c'+str(n)+'1']
            top = ['p'+str(n)+'1']
            include = []
            param1 = []
            param2 = []
            transform_param = []           
            data_param = []
            constructLayer(f, name, element, bot, top, include, param1, param2, \
                        transform_param, data_param)  
        randLayers.pop(0)   # pop the used layer
        
        # then the following layers are all fully connected layer
        # first add the concat layer:
        # get the merge layer
        ip_idx = 0
        if winNum > 1: # if there are split layers
            randLayer = 'Concat'
            name = 'concat'
            bot = []
            top = ['p1']
            data_param = []
            data_param.append('axis: 1')   # add dimension for concat
            for i in range(0, winNum):
                bot.append('p'+str(i)+str(1))   # append each data sections
            include = []
            param1 = []
            param2 = []
            transform_param = []
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                            transform_param, data_param)
        # for loop to write each layer
        for randLayer in randLayers:
            randLayer = randLayer[0]
            name = 'l'+str(ip_idx)
            if ip_idx == 0: # this means the layer is just up to data
                bot = ['p1']
            else:
                bot = ['l'+str(ip_idx-1)]
            top = ['l'+str(ip_idx)]
            include = []
            param1 = []
            param2 = []
            transform_param = []
            data_param = []
            if type(randLayer) == float:    
                pass # this means the layer is dropout and nothing need add
            elif type(randLayer) == int:    
                param1 = ['lr_mult: 1']
                param2 = ['lr_mult: 2']
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                        transform_param, data_param)    
            if ip_idx < len(randLayers)-1 and type(randLayer) == int: # if an ip layer
                # write relu layer
                f.write('layer {\n')  
                f.write('  name: "relu'+str(ip_idx)+'"\n')
                f.write('  type: "ReLU"\n')
                f.write('  bottom: "l'+str(ip_idx)+'"\n')
                f.write('  top: "l'+str(ip_idx)+'"\n')
                f.write('}\n')
            ip_idx = ip_idx+1
        # write the top layer - accuracy layer
        name = 'prob'
        randLayer = 'Softmax'
        bot = ['l'+str(ip_idx-1)]
        top = ['prob']
        include = ['phase: TEST']
        param1 = []
        param2 = []
        transform_param = []
        data_param = []
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)    
    
def constructPrototxtH(randLayersAll, prototxt_name_train, prototxt_name_test, \
                      prefix, subID, fold, scalar):  
    # first clean the file
    with open(caffe_root+caffe_example+prototxt_name_train+".prototxt", "r+") as f:
        lines = f.readlines()
        f.seek(0)
        for line in lines:
            f.write('')
        f.truncate()
    f.close()
    with open(caffe_root+caffe_example+prototxt_name_test+".prototxt", "r+") as f:
        lines = f.readlines()
        f.seek(0)
        for line in lines:
            f.write('')
        f.truncate()
    f.close()
    
    # get the copy of the layer info
    randLayers = list(randLayersAll)
    # then revise the file
    with open(caffe_root+caffe_example+prototxt_name_train+".prototxt", "r+") as f:
        # write files
        f.seek(0)
        # write title
        f.write('name: "emo"\n') 
        # write the first layer - data_train layer
        name = 'ct'
        randLayer = 'data_train'
        bot = []
        top = ['data', 'label']
        include = ['phase: TRAIN']
        param1 = []
        param2 = []
        transform_param = ['scale: '+str(scalar)]
        data_param1 = 'source: "'+caffe_root+caffe_example+\
                            prefix+str(subID)+'train_lmdb_'+str(fold)+'"'
        data_param2 = 'batch_size: 32'
        data_param3 = 'backend: LMDB'
        data_param = [data_param1, data_param2, data_param3]
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)        
        # write the second layer - data_test layer
        name = 'ct'
        randLayer = 'data_test'
        bot = []
        top = ['data', 'label']
        include = ['phase: TEST']
        param1 = []
        param2 = []
        transform_param = ['scale: '+str(scalar)]
        data_param1 = 'source: "'+caffe_root+caffe_example+\
                            prefix+str(subID)+'test_lmdb_'+str(fold)+'"'
        data_param2 = 'batch_size: 32'
        data_param3 = 'backend: LMDB'
        data_param = [data_param1, data_param2, data_param3]
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)    
        # write the third layer - split layer
        if len(randLayers[0]) == 1: # indicate this is the dropout layer
            randLayer = randLayers[0]
            randLayer = randLayer[0]
            name = 'drop'
            bot = ['data']
            top = ['data']
            include = []
            param1 = []
            param2 = []
            transform_param = []
            data_param = []
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                            transform_param, data_param) 
            # because the dropout layer has been added , so remove the first element
            randLayers.pop(0)
        # then add the split layer
        # first get the window number
        winNum = len(randLayers[0])
        if winNum > 1: # if there are split layers
            randLayer = 'Slice'
            name = 'slice_pair'
            bot = ['data']
            top = []
            data_param = []
            data_param.append('axis: 1')   # add dimension for slice
            for i in range(0, winNum):
                top.append('data'+str(i))
                # add slice point each time
                if i+1 < winNum:
                    data_param.append('slice_point: '+str(i+1))
            include = []
            param1 = []
            param2 = []
            transform_param = []
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                            transform_param, data_param) 
        # for loop to write each layer
        splitNum = 0
        # get the split layer number
        for randLayer in randLayers:
            if len(randLayer) > 1:
                splitNum += 1
        for ip_idx in range(0, splitNum):
            randLayer = randLayers[ip_idx]
            for i in range(0, len(randLayer)):
                element = randLayer[i]
                name = 'l'+str(i)+str(ip_idx)
                if ip_idx == 0: # this means the layer is at bottom
                    # use the sliced for bottom
                    bot = ['data'+str(i)]
                else:
                    bot = ['l'+str(i)+str(ip_idx-1)]
                top = ['l'+str(i)+str(ip_idx)]
                include = []
                param1 = []
                param2 = []
                transform_param = []
                data_param = []
                if type(element) == float:    
                    pass # this means the layer is dropout and nothing need add
                elif type(element) == int:    
                    # need to share the weights
                    param1 = ['name: "ip'+str(ip_idx)+'_w"', 'lr_mult: 1']
                    param2 = ['name: "ip'+str(ip_idx)+'_b"', 'lr_mult: 2']
                constructLayer(f, name, element, bot, top, include, param1, param2, \
                        transform_param, data_param)    
                if ip_idx < len(randLayers)-1 and type(element) == int: # if an ip layer
                    # write relu layer
                    f.write('layer {\n')  
                    f.write('  name: "relu'+str(i)+str(ip_idx)+'"\n')
                    f.write('  type: "ReLU"\n')
                    f.write('  bottom: "l'+str(i)+str(ip_idx)+'"\n')
                    f.write('  top: "l'+str(i)+str(ip_idx)+'"\n')
                    f.write('}\n')
                        
        # then get the merge layer
        if winNum > 1: # if there are split layers
            randLayer = 'Concat'
            name = 'concat'
            bot = []
            top = ['ll'+str(ip_idx)]
            data_param = []
            data_param.append('axis: 1')   # add dimension for concat
            for i in range(0, winNum):
                bot.append('l'+str(i)+str(ip_idx))
            include = []
            param1 = []
            param2 = []
            transform_param = []
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                            transform_param, data_param)                         
        # then add the fully connected layer
        for ip_idx in range(splitNum, len(randLayers)):
            randLayer = randLayers[ip_idx]
            element = randLayer[0]
            name = 'll'+str(ip_idx)
            if ip_idx == 0: # this means the layer is just up to data
                bot = ['data']
            else:
                bot = ['ll'+str(ip_idx-1)]
            top = ['ll'+str(ip_idx)]
            include = []
            param1 = []
            param2 = []
            transform_param = []
            data_param = []
            if type(element) == float:    
                pass # this means the layer is dropout and nothing need add
            elif type(element) == int:    
                param1 = ['lr_mult: 1']
                param2 = ['lr_mult: 2']
            constructLayer(f, name, element, bot, top, include, param1, param2, \
                        transform_param, data_param)    
            if ip_idx < len(randLayers)-1 and type(element) == int: # if an ip layer
                # write relu layer
                f.write('layer {\n')  
                f.write('  name: "relu'+str(ip_idx)+'"\n')
                f.write('  type: "ReLU"\n')
                f.write('  bottom: "ll'+str(ip_idx)+'"\n')
                f.write('  top: "ll'+str(ip_idx)+'"\n')
                f.write('}\n')
                
        # write the top layer - accuracy layer
        name = 'accuracy'
        randLayer = 'Accuracy'
        bot1 = 'll'+str(ip_idx)
        bot2 = 'label'
        bot = [bot1, bot2]
        top = ['accuracy']
        include = ['phase: TEST']
        param1 = []
        param2 = []
        transform_param = []
        data_param = []
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)   
        # write the top layer - loss layer
        name = 'loss'
        randLayer = 'SoftmaxWithLoss'
        bot1 = 'll'+str(ip_idx)
        bot2 = 'label'
        bot = [bot1, bot2]
        top = ['loss']
        include = []
        param1 = []
        param2 = []
        transform_param = []
        data_param = []
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)   
    
    # get the copy of the layer info
    randLayers = list(randLayersAll)
    # write test prototxt            
    with open(caffe_root+caffe_example+prototxt_name_test+".prototxt", "r+") as f:
        # write files
        f.seek(0)
        # write title
        f.write('name: "emo"\n') 
        # write the first layer - data_train layer
        f.write('input: "data"\n')  
        f.write('input_dim: 1\n')  
        f.write('input_dim: '+str(winNum)+'\n')  
        f.write('input_dim: blob_dim[1]\n')  
        f.write('input_dim: '+str(128/winNum)+'\n') 
        # write the third layer - split layer
        if len(randLayers[0]) == 1: # indicate this is the dropout layer
            randLayer = randLayers[0]            
            randLayer = randLayer[0]
            name = 'drop'
            bot = ['data']
            top = ['data']
            include = []
            param1 = []
            param2 = []
            transform_param = []
            data_param = []
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                            transform_param, data_param) 
            # because the dropout layer has been added , so remove the first element
            randLayers.pop(0)
        # then add the split layer    
        # first get the window number
        winNum = len(randLayers[0])
        if winNum > 1: # if there are split layers
            randLayer = 'Slice'
            name = 'slice_pair'
            bot = ['data']
            top = []
            data_param = []
            data_param.append('axis: 1')   # add dimension for slice
            for i in range(0, winNum):
                top.append('data'+str(i))
                # add slice point each time
                if i+1 < winNum:
                    data_param.append('slice_point: '+str(i+1))
            include = []
            param1 = []
            param2 = []
            transform_param = []
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                            transform_param, data_param) 
        # for loop to write each layer
        splitNum = 0
        # get the split layer number
        for randLayer in randLayers:
            if len(randLayer) > 1:
                splitNum += 1
        for ip_idx in range(0, splitNum):
            randLayer = randLayers[ip_idx]
            for i in range(0, len(randLayer)):
                element = randLayer[i]
                name = 'l'+str(i)+str(ip_idx)
                if ip_idx == 0: # this means the layer is at bottom
                    # use the sliced for bottom
                    bot = ['data'+str(i)]
                else:
                    bot = ['l'+str(i)+str(ip_idx-1)]
                top = ['l'+str(i)+str(ip_idx)]
                include = []
                param1 = []
                param2 = []
                transform_param = []
                data_param = []
                if type(element) == float:    
                    pass # this means the layer is dropout and nothing need add
                elif type(element) == int:    
                    # need to share the weights
                    param1 = ['name: "ip'+str(ip_idx)+'_w"', 'lr_mult: 1']
                    param2 = ['name: "ip'+str(ip_idx)+'_b"', 'lr_mult: 2']
                constructLayer(f, name, element, bot, top, include, param1, param2, \
                        transform_param, data_param)    
                if ip_idx < len(randLayers)-1 and type(element) == int: # if an ip layer
                    # write relu layer
                    f.write('layer {\n')  
                    f.write('  name: "relu'+str(i)+str(ip_idx)+'"\n')
                    f.write('  type: "ReLU"\n')
                    f.write('  bottom: "l'+str(i)+str(ip_idx)+'"\n')
                    f.write('  top: "l'+str(i)+str(ip_idx)+'"\n')
                    f.write('}\n')
                        
        # then get the merge layer
        if winNum > 1: # if there are split layers
            randLayer = 'Concat'
            name = 'concat'
            bot = []
            top = ['ll'+str(ip_idx)]
            data_param = []
            data_param.append('axis: 1')   # add dimension for concat
            for i in range(0, winNum):
                bot.append('l'+str(i)+str(ip_idx))
            include = []
            param1 = []
            param2 = []
            transform_param = []
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                            transform_param, data_param)                         
        # then add the fully connected layer
        for ip_idx in range(splitNum, len(randLayers)):
            randLayer = randLayers[ip_idx]
            element = randLayer[0]
            name = 'll'+str(ip_idx)
            if ip_idx == 0: # this means the layer is just up to data
                bot = ['data']
            else:
                bot = ['ll'+str(ip_idx-1)]
            top = ['ll'+str(ip_idx)]
            include = []
            param1 = []
            param2 = []
            transform_param = []
            data_param = []
            if type(element) == float:    
                pass # this means the layer is dropout and nothing need add
            elif type(element) == int:    
                param1 = ['lr_mult: 1']
                param2 = ['lr_mult: 2']
            constructLayer(f, name, element, bot, top, include, param1, param2, \
                        transform_param, data_param)    
            if ip_idx < len(randLayers)-1 and type(element) == int: # if an ip layer
                # write relu layer
                f.write('layer {\n')  
                f.write('  name: "relu'+str(ip_idx)+'"\n')
                f.write('  type: "ReLU"\n')
                f.write('  bottom: "ll'+str(ip_idx)+'"\n')
                f.write('  top: "ll'+str(ip_idx)+'"\n')
                f.write('}\n')
                
        # write the top layer - prob layer
        name = 'prob'
        randLayer = 'Softmax'
        bot = ['ll'+str(ip_idx)]
        top = ['prob']
        include = ['phase: TEST']
        param1 = []
        param2 = []
        transform_param = []
        data_param = []
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)   

def constructPrototxtCNN(randLayersAll, prototxt_name_train, prototxt_name_test, \
                        fold, scalar, blob_dim):
                          
    # first clean the file
    with open(caffe_root+caffe_example+prototxt_name_train+".prototxt", "r+") as f:
        lines = f.readlines()
        f.seek(0)
        for line in lines:
            f.write('')
        f.truncate()
    f.close()
    with open(caffe_root+caffe_example+prototxt_name_test+".prototxt", "r+") as f:
        lines = f.readlines()
        f.seek(0)
        for line in lines:
            f.write('')
        f.truncate()
    f.close()
    
    # get the copy of the layer info
    randLayers = list(randLayersAll)
    # then revise the file
    with open(caffe_root+caffe_example+prototxt_name_train+".prototxt", "r+") as f:
        # write files
        f.seek(0)
        # write title
        f.write('name: "emo"\n') 
        # write the first layer - data_train layer
        name = 'ct'
        randLayer = 'data_train'
        bot = []
        top = ['data', 'label']
        include = ['phase: TRAIN']
        param1 = []
        param2 = []
        transform_param = ['scale: '+str(scalar)]
        data_param1 = 'source: "'+caffe_root+caffe_example+'train_lmdb_'+str(fold)+'"'
        data_param2 = 'batch_size: 32'
        data_param3 = 'backend: LMDB'
        data_param = [data_param1, data_param2, data_param3]
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)        
        # write the second layer - data_test layer
        name = 'ct'
        randLayer = 'data_test'
        bot = []
        top = ['data', 'label']
        include = ['phase: TEST']
        param1 = []
        param2 = []
        transform_param = ['scale: '+str(scalar)]
        data_param1 = 'source: "'+caffe_root+caffe_example+'test_lmdb_'+str(fold)+'"'
        data_param2 = 'batch_size: 32'
        data_param3 = 'backend: LMDB'
        data_param = [data_param1, data_param2, data_param3]
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)    
        # write the third layer: convolution layer 1
        randLayer = randLayers[0]
        name = 'c1'
        bot = ['data']
        top = ['c1']
        include = []
        param1 = ['lr_mult: 1']
        param2 = ['lr_mult: 2']
        transform_param = []           
        data_param = []
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param) 
        randLayers.pop(0)   # pop the used layer
        # write the attached third layer: pooling layer 1        
        randLayer = randLayers[0]
        name = 'p1'
        bot = ['c1']
        top = ['p1']
        include = []
        param1 = []
        param2 = []
        transform_param = []           
        data_param = []
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)        
        randLayers.pop(0)   # pop the used layer
        # write the fourth layer: convolution layer 2
        randLayer = randLayers[0]
        bot_idx = 1
        if len(randLayer) == 3:
            name = 'c2'
            bot = ['p1']
            top = ['c2']
            include = []
            param1 = ['lr_mult: 1']
            param2 = ['lr_mult: 2']
            transform_param = []           
            data_param = []
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                        transform_param, data_param) 
            randLayers.pop(0)   # pop the used layer
            # write the attached fourth layer: pooling layer 2       
            randLayer = randLayers[0]
            name = 'p2'
            bot = ['c2']
            top = ['p2']
            include = []
            param1 = []
            param2 = []
            transform_param = []           
            data_param = []
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                        transform_param, data_param)  
            randLayers.pop(0)   # pop the used layer
            bot_idx = 2
        # then the following layers are all fully connected layer
        # for loop to write each layer
        ip_idx = 0
        for randLayer in randLayers:
            randLayer = randLayer[0]
            name = 'l'+str(ip_idx)
            if ip_idx == 0: # this means the layer is just up to data
                bot = ['p'+str(bot_idx)]
            else:
                bot = ['l'+str(ip_idx-1)]
            top = ['l'+str(ip_idx)]
            include = []
            param1 = []
            param2 = []
            transform_param = []
            data_param = []
            if type(randLayer) == float:    
                pass # this means the layer is dropout and nothing need add
            elif type(randLayer) == int:    
                param1 = ['lr_mult: 1']
                param2 = ['lr_mult: 2']
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                        transform_param, data_param)    
            if ip_idx < len(randLayers)-1 and type(randLayer) == int: # if an ip layer
                # write relu layer
                f.write('layer {\n')  
                f.write('  name: "relu'+str(ip_idx)+'"\n')
                f.write('  type: "ReLU"\n')
                f.write('  bottom: "l'+str(ip_idx)+'"\n')
                f.write('  top: "l'+str(ip_idx)+'"\n')
                f.write('}\n')
            ip_idx = ip_idx+1
        # write the top layer - accuracy layer
        name = 'accuracy'
        randLayer = 'Accuracy'
        bot1 = 'l'+str(ip_idx-1)
        bot2 = 'label'
        bot = [bot1, bot2]
        top = ['accuracy']
        include = ['phase: TEST']
        param1 = []
        param2 = []
        transform_param = []
        data_param = []
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)   
        # write the top layer - loss layer
        name = 'loss'
        randLayer = 'SoftmaxWithLoss'
        bot1 = 'l'+str(ip_idx-1)
        bot2 = 'label'
        bot = [bot1, bot2]
        top = ['loss']
        include = []
        param1 = []
        param2 = []
        transform_param = []
        data_param = []
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)   
        
    # get the copy of the layer info
    randLayers = list(randLayersAll)
    # then revise the file
    with open(caffe_root+caffe_example+prototxt_name_test+".prototxt", "r+") as f:
        # write files
        f.seek(0)
        # write title
        f.write('name: "emo"\n') 
        # write the first layer - data_train layer
        f.write('input: "data"\n')  
        f.write('input_dim: 1\n')  
        f.write('input_dim: '+str(blob_dim[0])+'\n')  
        f.write('input_dim: '+str(blob_dim[1])+'\n')  
        f.write('input_dim: '+str(blob_dim[2])+'\n')   
        # write the third layer: convolution layer 1
        randLayer = randLayers[0]
        name = 'c1'
        bot = ['data']
        top = ['c1']
        include = []
        param1 = ['lr_mult: 1']
        param2 = ['lr_mult: 2']
        transform_param = []           
        data_param = []
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param) 
        randLayers.pop(0)   # pop the used layer
        # write the attached third layer: pooling layer 1        
        randLayer = randLayers[0]
        name = 'p1'
        bot = ['c1']
        top = ['p1']
        include = []
        param1 = []
        param2 = []
        transform_param = []           
        data_param = []
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)        
        randLayers.pop(0)   # pop the used layer
        # write the fourth layer: convolution layer 2
        randLayer = randLayers[0]
        bot_idx = 1
        if len(randLayer) == 3:
            name = 'c2'
            bot = ['p1']
            top = ['c2']
            include = []
            param1 = ['lr_mult: 1']
            param2 = ['lr_mult: 2']
            transform_param = []           
            data_param = []
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                        transform_param, data_param) 
            randLayers.pop(0)   # pop the used layer
            # write the attached fourth layer: pooling layer 2       
            randLayer = randLayers[0]
            name = 'p2'
            bot = ['c2']
            top = ['p2']
            include = []
            param1 = []
            param2 = []
            transform_param = []           
            data_param = []
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                        transform_param, data_param)  
            randLayers.pop(0)   # pop the used layer
            bot_idx = 2
        # then the following layers are all fully connected layer
        # for loop to write each layer
        ip_idx = 0
        for randLayer in randLayers:
            randLayer = randLayer[0]
            name = 'l'+str(ip_idx)
            if ip_idx == 0: # this means the layer is just up to data
                bot = ['p'+str(bot_idx)]
            else:
                bot = ['l'+str(ip_idx-1)]
            top = ['l'+str(ip_idx)]
            include = []
            param1 = []
            param2 = []
            transform_param = []
            data_param = []
            if type(randLayer) == float:    
                pass # this means the layer is dropout and nothing need add
            elif type(randLayer) == int:    
                param1 = ['lr_mult: 1']
                param2 = ['lr_mult: 2']
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                        transform_param, data_param)    
            if ip_idx < len(randLayers)-1 and type(randLayer) == int: # if an ip layer
                # write relu layer
                f.write('layer {\n')  
                f.write('  name: "relu'+str(ip_idx)+'"\n')
                f.write('  type: "ReLU"\n')
                f.write('  bottom: "l'+str(ip_idx)+'"\n')
                f.write('  top: "l'+str(ip_idx)+'"\n')
                f.write('}\n')
            ip_idx = ip_idx+1
        # write the top layer - prob layer
        name = 'prob'
        randLayer = 'Softmax'
        bot = ['l'+str(ip_idx-1)]
        top = ['prob']
        include = ['phase: TEST']
        param1 = []
        param2 = []
        transform_param = []
        data_param = []
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)   
    
def constructPrototxt(randLayers, prototxt_name_train, prototxt_name_test, \
                      fold, scalar, blob_dim):
                          
    # first clean the file
    with open(caffe_root+caffe_example+prototxt_name_train+".prototxt", "r+") as f:
        lines = f.readlines()
        f.seek(0)
        for line in lines:
            f.write('')
        f.truncate()
    f.close()
    with open(caffe_root+caffe_example+prototxt_name_test+".prototxt", "r+") as f:
        lines = f.readlines()
        f.seek(0)
        for line in lines:
            f.write('')
        f.truncate()
    f.close()
    
    # then revise the file
    with open(caffe_root+caffe_example+prototxt_name_train+".prototxt", "r+") as f:
        # write files
        f.seek(0)
        # write title
        f.write('name: "emo"\n') 
        # write the first layer - data_train layer
        name = 'ct'
        randLayer = 'data_train'
        bot = []
        top = ['data', 'label']
        include = ['phase: TRAIN']
        param1 = []
        param2 = []
        transform_param = ['scale: '+str(scalar)]
        data_param1 = 'source: "'+caffe_root+caffe_example+\
                            'train_lmdb_'+str(fold)+'"'
        data_param2 = 'batch_size: 32'
        data_param3 = 'backend: LMDB'
        data_param = [data_param1, data_param2, data_param3]
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)        
        # write the second layer - data_test layer
        name = 'ct'
        randLayer = 'data_test'
        bot = []
        top = ['data', 'label']
        include = ['phase: TEST']
        param1 = []
        param2 = []
        transform_param = ['scale: '+str(scalar)]
        data_param1 = 'source: "'+caffe_root+caffe_example+\
                            'test_lmdb_'+str(fold)+'"'
        data_param2 = 'batch_size: 32'
        data_param3 = 'backend: LMDB'
        data_param = [data_param1, data_param2, data_param3]
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)    
        # for loop to write each layer
        ip_idx = 0
        for randLayer in randLayers:
            name = 'l'+str(ip_idx)
            if ip_idx == 0: # this means the layer is just up to data
                bot = ['data']
            else:
                bot = ['l'+str(ip_idx-1)]
            top = ['l'+str(ip_idx)]
            include = []
            param1 = []
            param2 = []
            transform_param = []
            data_param = []
            if type(randLayer) == float:    
                pass # this means the layer is dropout and nothing need add
            elif type(randLayer) == int:    
                param1 = ['lr_mult: 1']
                param2 = ['lr_mult: 2']
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                        transform_param, data_param)    
            if ip_idx < len(randLayers)-1 and type(randLayer) == int: # if an ip layer
                # write relu layer
                f.write('layer {\n')  
                f.write('  name: "relu'+str(ip_idx)+'"\n')
                f.write('  type: "ReLU"\n')
                f.write('  bottom: "l'+str(ip_idx)+'"\n')
                f.write('  top: "l'+str(ip_idx)+'"\n')
                f.write('}\n')
            ip_idx = ip_idx+1
        # write the top layer - accuracy layer
        name = 'accuracy'
        randLayer = 'Accuracy'
        bot1 = 'l'+str(ip_idx-1)
        bot2 = 'label'
        bot = [bot1, bot2]
        top = ['accuracy']
        include = ['phase: TEST']
        param1 = []
        param2 = []
        transform_param = []
        data_param = []
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)   
        # write the top layer - loss layer
        name = 'loss'
        randLayer = 'SoftmaxWithLoss'
        bot1 = 'l'+str(ip_idx-1)
        bot2 = 'label'
        bot = [bot1, bot2]
        top = ['loss']
        include = []
        param1 = []
        param2 = []
        transform_param = []
        data_param = []
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)   
        
    # write test prototxt            
    with open(caffe_root+caffe_example+prototxt_name_test+".prototxt", "r+") as f:
        # write files
        f.seek(0)
        # write title
        f.write('name: "emo"\n') 
        # write the first layer - data_train layer
        f.write('input: "data"\n')  
        f.write('input_dim: 1\n')  
        f.write('input_dim: '+str(blob_dim[0])+'\n')  
        f.write('input_dim: '+str(blob_dim[1])+'\n')  
        f.write('input_dim: '+str(blob_dim[2])+'\n') 
        # for loop to write each layer
        ip_idx = 0
        for randLayer in randLayers:
            name = 'l'+str(ip_idx)
            if ip_idx == 0: # this means the layer is just up to data
                bot = ['data']
            else:
                bot = ['l'+str(ip_idx-1)]
            top = ['l'+str(ip_idx)]
            include = []
            param1 = []
            param2 = []
            transform_param = []
            data_param = []
            if type(randLayer) == float:    
                pass # this means the layer is dropout and nothing need add
            elif type(randLayer) == int:    
                param1 = ['lr_mult: 1']
                param2 = ['lr_mult: 2']
            constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                        transform_param, data_param)    
            if ip_idx < len(randLayers)-1 and type(randLayer) == int: # if an ip layer
                # write relu layer
                f.write('layer {\n')  
                f.write('  name: "relu'+str(ip_idx)+'"\n')
                f.write('  type: "ReLU"\n')
                f.write('  bottom: "l'+str(ip_idx)+'"\n')
                f.write('  top: "l'+str(ip_idx)+'"\n')
                f.write('}\n')            
            ip_idx = ip_idx+1
        # write the top layer - prob layer
        name = 'prob'
        randLayer = 'Softmax'
        bot = ['l'+str(ip_idx-1)]
        top = ['prob']
        include = ['phase: TEST']
        param1 = []
        param2 = []
        transform_param = []
        data_param = []
        constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param)   
        
def constructLayer(f, name, randLayer, bot, top, include, param1, param2, \
                    transform_param, data_param):
    if not name:
        print('name or type should not be empty')
        return
    f.write('layer {\n')        
    f.write('  name: "'+name+'"\n')
    if randLayer == 'data_train' or randLayer == 'data_test':  # the data layer
        f.write('  type: "'+'Data'+'"\n')
    elif type(randLayer) == int:
        f.write('  type: "'+'InnerProduct'+'"\n')
    elif type(randLayer) == float:
        f.write('  type: "'+'Dropout'+'"\n')
    elif type(randLayer) == list:
        if len(randLayer) == 3:
            f.write('  type: "'+'Convolution'+'"\n')
        elif len(randLayer) == 2:
            f.write('  type: "'+'Pooling'+'"\n')
    else:
        f.write('  type: "'+randLayer+'"\n')
    # add bottum
    if bot:
        for t in bot:
            f.write('  bottom: "'+t+'"\n')
    # add top
    if top:
        for t in top:
            f.write('  top: "'+t+'"\n')
    # write include
    if include:
        f.write('  include {\n')
        for t in include:
            f.write('    '+t+'\n')
        f.write('  }\n')
    # write param1
    if param1:
        f.write('  param {\n')
        for t in param1:
            f.write('    '+t+'\n')
        f.write('  }\n')
    # write param2
    if param2:
        f.write('  param {\n')
        for t in param2:
            f.write('    '+t+'\n')
        f.write('  }\n')    
    # write transform_param
    if transform_param:
        f.write('  transform_param {\n')
        for t in transform_param:
            f.write('    '+t+'\n')
        f.write('  }\n')  
    # write data_param
    if data_param:
        if randLayer == 'Slice':
            f.write('  slice_param {\n')
            for t in data_param:
                f.write('    '+t+'\n')
            f.write('  }\n')                
        elif randLayer == 'Concat':
            f.write('  concat_param {\n')
            for t in data_param:
                f.write('    '+t+'\n')
            f.write('  }\n')
        else:
            f.write('  data_param {\n')
            for t in data_param:
                f.write('    '+t+'\n')
            f.write('  }\n') 
    if type(randLayer) == int:
        f.write('  inner_product_param {\n')
        f.write('    num_output: '+str(randLayer)+'\n')
        f.write('    weight_filler {\n')
        f.write('      type: "xavier"\n')
        f.write('    }\n')
        f.write('    bias_filler {\n') 
        f.write('      type: "constant"\n') 
        f.write('    }\n')
        f.write('  }\n')
    elif type(randLayer) == float:
        f.write('  dropout_param {\n')
        f.write('    dropout_ratio: '+str(randLayer)+'\n')
        f.write('  }\n')
    elif type(randLayer) == list:
        if len(randLayer) == 3:
            f.write('  convolution_param {\n')
            f.write('    num_output: '+str(randLayer[2])+'\n')
            f.write('    kernel_h: '+str(randLayer[0])+'\n')
            f.write('    kernel_w: '+str(randLayer[1])+'\n')
            f.write('    stride: 1\n')
            f.write('    weight_filler {\n')
            f.write('      type: "xavier"\n')
            f.write('    }\n')
            f.write('    bias_filler {\n') 
            f.write('      type: "constant"\n') 
            f.write('    }\n')
            f.write('  }\n')
        elif len(randLayer) == 2:   # write the pooling layer
            f.write('  pooling_param {\n')
            f.write('    pool: MAX\n')
            f.write('    kernel_h: '+str(randLayer[0])+'\n')
            f.write('    kernel_w: '+str(randLayer[1])+'\n')
            f.write('    stride: 2\n')
            f.write('  }\n')
        
    f.write('}\n') 
    