# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 19:42:19 2015

@author: zijing
"""

import scipy
import h5py
import numpy as np

# Make sure that caffe is on the python path:
caffe_root = '/home/zijing/caffe/'  # this file is expected to be in {caffe_root}/examples
caffe_example= 'examples/DLAutoTest/'
import sys
sys.path.insert(0, caffe_root + 'python')
sys.path.append(caffe_root)
from examples.DLAutoTest import mat2lmdb
from examples.DLAutoTest import transferModule

'''
These tests are based on models proposed by:

paper [1]:  Krizhevsky, Alex, Ilya Sutskever, and Geoffrey E. Hinton. "Imagenet classification with deep convolutional neural   	    networks." Advances in neural information processing systems. 2012.
paper [2]:  Szegedy, Christian, et al. "Going deeper with convolutions." arXiv preprint arXiv:1409.4842 (2014).
'''  

'''
The data is in the format with variable name:
    train_x, test_x, train_y, test_y
    x is feature, y is label
    there is no model selection, therefore we don't use validation set

The data size should be feature: 4-D, label: 1-D

The feature dimension should be:
    [epochs * featureMaps * channels * timeSamples]

The label dimension should be:
    [epochs,]
'''

maxFold = 15

for dataIdx in range(0, maxFold):
    # load data first
    data = '/home/zijing/Downloads/p'+str(dataIdx)+'.mat'
    f = h5py.File(data)
    train_x = f[u"train_x"]
    train_x = np.array(train_x)
    train_y = f[u"train_y"]
    train_y = np.array(train_y)
    test_x  = f[u"test_x"]
    test_x  = np.array(test_x)
    test_y  = f[u"test_y"]
    test_y  = np.array(test_y)
    train_x = train_x.transpose()
    train_y = train_y.transpose()
    train_y = train_y[:,0]
    test_x = test_x.transpose()
    test_y = test_y.transpose()
    test_y = test_y[:,0]
    
    #f = h5py.File(data)
    #Inputs = f['Inputs'][:]
    #Labels = f['Labels'][:]    
    
    blob_dim = list(train_x.shape)
    # check the blob dimension with [featureMaps * channels * timeSamples]
    blob_dim.pop(0)
    
    mat2lmdb.data2lmdb(train_x, test_x, train_y, test_y, dataIdx, '_lmdb_alex_')
    
    ###########################################################################
    print('=' * 78)
    print('Current feature choose: ' + str(dataIdx))
    print('AlexNet as model')
    print('=' * 78)    
    
    iterations = 500
    scalar = 0.1
    transferMod = transferModule.transferModule(caffe_root, caffe_example, 'alexNet')    
    transferMod.edit_prototxt_iter('alexNetSolver', iterations)    
    
    ''' modify model in file:
        alexNetSolver.prototxt
        alexNetTrain.prototxt
        alexNetTest.prototxt
    '''
    # auc_all = np.zeros([maxFold, 20]) 
    fold = dataIdx
            
    transferMod.train_baseline_model('alexNetSolver', \
                                     'alexNetTrain', fold, scalar, '_lmdb_alex_')
    auc = np.zeros([20])
    transferMod.edit_model_test('alexNetTest', blob_dim)
    for i in range(1,21):
        auc[i-1]    =   transferMod.get_accuracy\
                        (train_x, test_x, train_y, test_y, \
                        'alexNetTest', fold, scalar, iterations*i, 'auc')   
    
    # write to mat file
    scipy.io.savemat(caffe_root+caffe_example+'alexNet'+str(dataIdx)+'.mat', \
                    {'auc': auc})
                    
    print('=' * 78)   
    ###########################################################################
    print('\n' * 2)
    
    ###########################################################################
    print('=' * 78)
    print('Current feature choose: ' + str(dataIdx))
    print('GoogleNet as model')
    print('=' * 78)    
    
    iterations = 1000
    scalar = 0.1
    transferMod = transferModule.transferModule(caffe_root, caffe_example, 'googleNet')    
    transferMod.edit_prototxt_iter('googleNetSolver', iterations)    
    
    ''' modify model in file:
        alexNetSolver.prototxt
        alexNetTrain.prototxt
        alexNetTest.prototxt
    '''
    # auc_all = np.zeros([maxFold, 20]) 
    fold = dataIdx
            
    res = transferMod.train_baseline_model('googleNetSolver', \
                                     'googleNetTrain', fold, scalar, '_lmdb_alex_')
    auc = np.zeros([20])
    transferMod.edit_model_test('googleNetTest', blob_dim)
    for i in range(1,21):
        auc[i-1]    =   transferMod.get_accuracy\
                        (train_x, test_x, train_y, test_y, \
                        'googleNetTest', fold, scalar, iterations*i, 'auc')   
        
    # write to mat file
    scipy.io.savemat(caffe_root+caffe_example+'googleNet'+str(dataIdx)+'.mat', \
                    {'auc': auc})
                    
    print('=' * 78)   
    ###########################################################################
    print('\n' * 2)
