# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 09:26:38 2015

@author: zijing
"""

import numpy as np
import scipy

# Make sure that caffe is on the python path:
caffe_root = '/home/zijing/caffe/'  # this file is expected to be in {caffe_root}/examples
caffe_example= 'examples/DLAutoTest/'
import sys
sys.path.insert(0, caffe_root + 'python')

subjectsTotalNum = 15;
prefix = 'googleNet'
b = np.zeros([subjectsTotalNum, 20])
for i in range(0, subjectsTotalNum):
    matAUC = scipy.io.loadmat(caffe_root+caffe_example+prefix+str(i)+'.mat')
    allAUC[i-1] = matAUC['auc']
    
avgAUC = np.mean(allAUC, axis=0)
