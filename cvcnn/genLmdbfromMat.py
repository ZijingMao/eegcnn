# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 19:42:19 2015

@author: zijing
"""

import scipy
import h5py
import numpy as np

# Make sure that caffe is on the python path:
caffe_root = '/home/zijing/caffe/'  # this file is expected to be in {caffe_root}/examples
caffe_example= 'examples/DLAutoTest/'
import sys
sys.path.insert(0, caffe_root + 'python')
sys.path.append(caffe_root)
from examples.DLAutoTest import mat2lmdb
from examples.DLAutoTest import transferModule

'''
The data is in the format with variable name:
    train_x, test_x, train_y, test_y
    x is feature, y is label
    there is no model selection, therefore we don't use validation set

The data size should be feature: 4-D, label: 1-D

The feature dimension should be:
    [epochs * featureMaps * channels * timeSamples]

The label dimension should be:
    [epochs,]
'''

# load data first
data = '/home/zijing/Downloads/eegdata.mat'
f = h5py.File(data)
train_x = f[u"train_x"]
train_x = np.array(train_x)
train_y = f[u"train_y"]
train_y = np.array(train_y)
test_x  = f[u"test_x"]
test_x  = np.array(test_x)
test_y  = f[u"test_y"]
test_y  = np.array(test_y)
train_x = train_x.transpose()
train_y = train_y.transpose()
train_y = train_y[:,0]
test_x = test_x.transpose()
test_y = test_y.transpose()
test_y = test_y[:,0]

#f = h5py.File(data)
#Inputs = f['Inputs'][:]
#Labels = f['Labels'][:]    

blob_dim = list(train_x.shape)
# check the blob dimension with [featureMaps * channels * timeSamples]
blob_dim.pop(0)

mat2lmdb.data2lmdb(train_x, test_x, train_y, test_y, dataIdx, '_lmdb_alex_')

