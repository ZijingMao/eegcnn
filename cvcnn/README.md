## cvcnn: *Computer vision CNN models*

We used two CV models to test EEG.

AlexNet

GoogleNet

### Introduction

We also want to test if classical computer vision models can also fit EEG signals.

### Modify cvcnn for eeg signals

0. **Prerequisites** 
  0. Caffe's [prerequisites](http://caffe.berkeleyvision.org/installation.html#prequequisites)
0. **Install Caffe** (this is the most complicated part)
  0. Download [Caffe v0.999](https://github.com/BVLC/caffe/archive/v0.999.tar.gz)
  0. Follow the [Caffe installation instructions](http://caffe.berkeleyvision.org/installation.html)
0. **Resize eeg signals**
  0. use either MATLAB or Python to resize your eeg signal to 227 by 227
0. **Modify model config**
  0. change bash file .sh with '--solver=/correct/directory/Solver.prototxt'
  0. change solver.prototxt with correct directory
  0. change Train.prototxt with correct directory
  0. change train_imageNet.py with correct directory
  0. change genLmdbfromMat.py with correct directory (option, skip if you don't use bash)

### Running cvcnn on eeg dataset

Let's assume you have set every configuration and directory correct, now:

1. Run generate lmdb database file on Python: `runfile('genLmdbfromMat.py')`
2. Choose one bash file to execute: `./your/directory/yourNet.sh` as verification of no issue for prototxt
3. Run the batch code for mutilple eeg dataset

**Note:** Most of the time an error happens is due to the directory is not set correctly

